﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using System.Configuration;

namespace Tracker
{
    /// <summary>
    /// Manages the connection of a kinect sensor and draws the received video and skeleton frames
    /// 
    /// Kinect sensor management using the KinectSensorChooser based on example http://code.msdn.microsoft.com/windowsdesktop/Beginning-Kinect-for-a198d400
    /// 
    /// Skeleton drawing based on MainWindow.xaml.cs from IGS PSE project
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        ///     Culture setting used to convert decimal values from configuration file
        /// </summary>
        public System.Globalization.CultureInfo configCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

        /// <summary>
        ///     Width of image
        /// </summary>
        private const float imageWidth = 640.0f;

        /// <summary>
        ///     Height of image
        /// </summary>
        private const float imageHeight = 480.0f;

        /// <summary>
        ///     Thickness of drawn joint lines
        /// </summary>
        private const double jointThickness = 3;

        /// <summary>
        ///     Thickness of body center ellipse
        /// </summary>
        private const double bodyCenterThickness = 10;

        /// <summary>
        ///     Thickness of clip edge rectangles
        /// </summary>
        private const double clipBoundsThickness = 1;

        /// <summary>
        ///     Brush used to draw skeleton center point
        /// </summary>
        private readonly Brush _centerPointBrush = Brushes.Blue;
        private readonly Brush _centerPointBrushActive = Brushes.Green;

        /// <summary>
        ///     Pen used for drawing bones that are currently inferred
        /// </summary>
        private readonly Pen _inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        ///     Brush used for drawing joints that are currently inferred
        /// </summary>
        private readonly Brush _inferredJointBrush = Brushes.Yellow;

        /// <summary>
        ///     Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen _trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        ///     Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush _trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        ///     Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        ///     Displayed image
        /// </summary>
        private DrawingImage imageSource;

        private KinectSensorChooser sensorChooser = null;
        private KinectSensor sensor = null;

        private UserTracker tracker = null;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += WindowLoaded;
        }

        // handle connection / switch of kinect sensor
        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs e)
        {
            var old = e.OldSensor;
            StopKinect(old);

            this.sensor = e.NewSensor;
            if (this.sensor == null)
            {
                return;
            }

            /* from "Joint Filtering" (http://msdn.microsoft.com/en-us/library/jj131024.aspx):
                Smoothing
                    Smoothing parameter. Increasing the smoothing parameter value leads to more highly-smoothed skeleton position values being returned.
                    It is the nature of smoothing that, as the smoothing value is increased, responsiveness to the raw data decreases.
                    Thus, increased smoothing leads to increased latency in the returned skeleton values.
                    Values must be in the range 0 through 1.0. Passing 0 causes the raw data to be returned.
                Correction
                    Correction parameter. Lower values are slower to correct towards the raw data and appear smoother, while higher values will correct toward the raw data more quickly.
                    Values must be in the range 0 through 1.0.
                Prediction
                    The number of frames to predict into the future.
                    Values must be greater than or equal to zero.
                    Values greater than 0.5 will likely lead to overshooting when moving quickly. This effect can be damped by using small values of fMaxDeviationRadius.
                JitterRadius (position compared to smoothed position of previous frame)
                    The radius in meters for jitter reduction.
                    Any jitter beyond this radius is clamped to the radius.
                MaxDeviationRadius (position compared to new smoothed position of this frame)
                    The maximum radius in meters that filtered positions are allowed to deviate from raw data.
                    Filtered values that would be more than this radius from the raw data are clamped at this distance, in the direction of the filtered value.
            */

            // Some smoothing with little latency (defaults).
            // Only filters out small jitters.
            // Good for gesture recognition in games.
            TransformSmoothParameters smoothingParam = new TransformSmoothParameters();
            {
                smoothingParam.Smoothing = Convert.ToSingle(ConfigurationManager.AppSettings["Smoothing"], configCulture);
                smoothingParam.Correction = Convert.ToSingle(ConfigurationManager.AppSettings["SmoothingCorrection"], configCulture); // seams to overshoot quite a bit, rather avoid as lag is not that bad in this use case
                smoothingParam.Prediction = Convert.ToSingle(ConfigurationManager.AppSettings["SmoothingPrediction"], configCulture);
                // maximum hand speed seams to be about 1.5m/s according to recorded data even when waving fast
                smoothingParam.JitterRadius = Convert.ToSingle(ConfigurationManager.AppSettings["SmoothingJitterRadius"], configCulture); // margin + maximum velocity / fps
                smoothingParam.MaxDeviationRadius = Convert.ToSingle(ConfigurationManager.AppSettings["SmoothingMaxDeviationRadius"], configCulture); // margin + maximum velocity / fps
            };

            // enable skeleton tracking
            if(Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSmoothing"])) {
                this.sensor.SkeletonStream.Enable(smoothingParam); throw new ExecutionEngineException();
            } else {
                this.sensor.SkeletonStream.Enable();
            }
            
            // track only upper body
            //this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
            this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;

            this.sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            this.sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            
            // add receivers of kinect data here
            this.sensor.AllFramesReady += this.SensorAllFramesReady;
            this.tracker.Sensor = this.sensor;

            try
            {
                sensor.Start();
                Status.Text = "Kinect Started";
            }
            catch (System.IO.IOException)
            {
                Status.Text = "Kinect Not Started";
                //maybe another app is using Kinect 
                this.sensorChooser.TryResolveConflict();
            } 
        }

        /// <summary>
        ///     Stop any running kinect sensor
        /// </summary>
        /// <param name="sensor">sensor to stop</param>
        private void StopKinect(KinectSensor sensor)
        {
            if (sensor != null)
            {
                if (sensor.IsRunning)
                {
                    sensor.Stop();
                    sensor.AudioSource.Stop();
                }
            }
        }

        /// <summary>
        ///     Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.Status.Text = "<- Please connect a Kinect!";

            // Create the drawing group we'll use for drawing
            drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            imageSource = new DrawingImage(drawingGroup);

            // Display the drawing using our image control
            Image.Source = imageSource;

            // Initialise user tracker
            this.tracker = new UserTracker();

            this.sensorChooser = new KinectSensorChooser();
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooserUi.KinectSensorChooser = sensorChooser;
            this.sensorChooser.Start();
        }

        /// <summary>
        ///     Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="mouseButtonEventArgs"></param>
        private void WindowClosing(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            StopKinect(this.sensor);
            this.sensorChooser.Stop();
            Environment.Exit(1);
        }

        /// <summary>
        ///     Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorAllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            
            this.DrawImage(sender, e);
            
        }

        private void KinectSensorChooserUI_Loaded(object sender, RoutedEventArgs e)
        {

        }

        // Skeleton Drawing --------------------------------------------------------------------

        /// <summary>
        ///     Draws the image of the kinect data
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">all frames ready event arguments</param>
        private void DrawImage(object sender, AllFramesReadyEventArgs e)
        {
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame == null)
                {
                    return;
                }
                byte[] pixels = new byte[colorFrame.PixelDataLength];
                colorFrame.CopyPixelDataTo(pixels);

                int stride = colorFrame.Width * 4;

                // collect skeleton data
                Skeleton[] skeletons = new Skeleton[0];
                using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
                {
                    if (skeletonFrame != null)
                    {
                        skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                        skeletonFrame.CopySkeletonDataTo(skeletons);
                    }
                }

                // draw image
                using (DrawingContext dc = drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawImage(
                        BitmapSource.Create(colorFrame.Width, colorFrame.Height, 96, 96, PixelFormats.Bgr32, null,
                                            pixels, stride), new Rect(0.0, 0.0, imageWidth, imageHeight));

                    if (skeletons.Length != 0)
                    {
                        // get activated skeletons from tracker to display a skeleton's state
                        var activeSkeletons = this.tracker.ActiveSkeletonIds;

                        foreach (Skeleton skeleton in skeletons)
                        {
                            RenderClippedEdges(skeleton, dc);
                            if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                            {
                                DrawBonesAndJoints(skeleton, dc, e.OpenDepthImageFrame());
                                dc.DrawEllipse(
                                    activeSkeletons.Contains(skeleton.TrackingId) ? _centerPointBrushActive : _centerPointBrush,
                                    null,
                                    SkeletonPointToScreen(skeleton.Position),
                                    bodyCenterThickness,
                                    bodyCenterThickness);
                            }
                            else if (skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                            {
                                dc.DrawEllipse(
                                    _centerPointBrush,
                                    null,
                                    SkeletonPointToScreen(skeleton.Position),
                                    bodyCenterThickness,
                                    bodyCenterThickness);
                            }
                        }
                    }

                    // prevent drawing outside of our render area
                    drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, imageWidth, imageHeight));
                }
            }
        }

        /// <summary>
        ///     Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, imageHeight - clipBoundsThickness, imageWidth, clipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, imageWidth, clipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, clipBoundsThickness, imageHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(imageWidth - clipBoundsThickness, 0, clipBoundsThickness, imageHeight));
            }
        }

        /// <summary>
        ///     Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="depth">depth information</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext, DepthImageFrame depth)
        {
            // Render Torso
            DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter, depth);
            DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight, depth);
            DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine, depth);
            DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter, depth);
            DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight, depth);

            // Left Arm
            DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft, depth);

            // Right Arm
            DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight, depth);
            DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight, depth);
            DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight, depth);

            // Left Leg
            DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft, depth);
            DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft, depth);

            // Right Leg
            DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight, depth);
            DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight, depth);
            DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight, depth);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = _trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = _inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, SkeletonPointToScreen(joint.Position), jointThickness,
                                               jointThickness);
                }
            }
            // dispose image frame to avoid warning about undisposed image frame
            if (depth != null)
            {
                depth.Dispose();
            }
        }

        /// <summary>
        ///     Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint,
                                                                                              DepthImageFormat
                                                                                                  .Resolution640x480Fps30);
            ColorImagePoint color = this.sensor.CoordinateMapper.MapDepthPointToColorPoint(DepthImageFormat.Resolution640x480Fps30, depthPoint, ColorImageFormat.RawBayerResolution640x480Fps30);


            return new Point(color.X, color.Y);
        }

        /// <summary>
        ///     Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        /// <param name="depth">depth information</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0,
                              JointType jointType1, DepthImageFrame depth)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = _inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = _trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, SkeletonPointToScreen(joint0.Position), SkeletonPointToScreen(joint1.Position));
        }
        // END Skeleton Drawing ------------------------------------------------------------------------
    }
}
