﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tracker
{
    /// <summary>
    ///     cycles through detected sceletons checking for the activation gesture and tracks activated skeletons
    /// </summary>
    class UserTracker
    {
        private KinectSensor sensor;
        private WebClient webClient = new WebClient();

        /// <summary>
        ///     array holding all skeleton data
        /// </summary>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/jj131025"/>
        private Skeleton[] skeletonData;

        /// <summary>
        ///     maximum number of simultaneously tracked skeletons, determined by the kinect sdk
        /// </summary>
        private const int MaximumNumberOfTrackedSkeletons = 2;

        /// <summary>
        ///     array holding ids of actively tracked skeletons
        /// </summary>
        private List<int> trackedSkeletons = new List<int>(MaximumNumberOfTrackedSkeletons);

        /// <summary>
        ///     array holding ids of deactivated skeletons (tracking finished)
        /// </summary>
        private Dictionary<int, TrackedUser> deactivatedSkeletons = new Dictionary<int, TrackedUser>();

        /// <summary>
        ///     array holding ids of activated skeletons (by activation gesture)
        /// </summary>
        private Dictionary<int, TrackedUser> activatedSkeletons = new Dictionary<int, TrackedUser>(MaximumNumberOfTrackedSkeletons);
         

        /// <summary>
        ///     queue of ids of skeletons not tracked, waiting to be checked for activation gesture
        /// </summary>
        private Queue<int> skeletonQueue = new Queue<int>();

        public UserTracker()
        {
        }

        /// <summary>
        ///     injector for the used kinect sensor
        /// </summary>
        public KinectSensor Sensor
        {
            get
            {
                return sensor;
            }
            set
            {
                // logic for changed sensor here
                sensor = value;
                this.skeletonData = new Skeleton[sensor.SkeletonStream.FrameSkeletonArrayLength];
                if (!sensor.SkeletonStream.AppChoosesSkeletons)
                {
                    sensor.SkeletonStream.AppChoosesSkeletons = true;
                }
                sensor.SkeletonFrameReady += this.SkeletonFrameReady;
            }
        }

        /// <summary>
        ///     list of actively tracked skeleton ids
        /// </summary>
        public List<int> ActiveSkeletonIds
        {
            get
            {
                return activatedSkeletons.Keys.ToList();
            }
        }
        
        /// <summary>
        ///     receive and use skeleton data from kinect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">sekeleton frame data from kinect</param>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/jj131025"/>
        private void SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            long timestamp = 0;
            bool skeletonFrameSet = false;

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame()) // Open the Skeleton frame
            {
                if (skeletonFrame != null && this.skeletonData != null) // check that a frame is available
                {
                    skeletonFrame.CopySkeletonDataTo(this.skeletonData); // get the skeletal information in this frame
                    skeletonFrameSet = true;
                    timestamp = skeletonFrame.Timestamp;
                }
            }

            if (!skeletonFrameSet)
            {
                return;
            }

            foreach (Skeleton skeleton in this.skeletonData)
            {

                if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                {
                    if (this.activatedSkeletons.ContainsKey(skeleton.TrackingId))
                    {
                        if (!this.StayActive(skeleton))
                        {
                            this.DeactivateSkeleton(skeleton.TrackingId);
                        }
                        else
                        {
                            TrackedUser user;
                            this.activatedSkeletons.TryGetValue(skeleton.TrackingId, out user);
                            user.AddSample(skeleton, timestamp);
                        }
                        // TODO limit tracking time
                    }
                    else
                    {
                        // check for activation gesture
                        if (this.DoesActivationGesture(skeleton))
                        {
                            this.ActivateSkeleton(skeleton);
                        }
                        else
                        {
                            // if not doing activation gesture, remove from list of tracked skeletons
                            // so that other skeletons can be tracked
                            //Console.WriteLine(DateTime.Now + " Stop tracking skeleton " + skeleton.TrackingId + " as of not doing activation gesture.");
                            this.trackedSkeletons.Remove(skeleton.TrackingId);
                        }
                    }
                }
                else if (skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                {
                    if (!this.skeletonQueue.Contains(skeleton.TrackingId))
                    {
                        //Console.WriteLine(DateTime.Now + " Enqueue skeleton " + skeleton.TrackingId + " to waiting list.");
                        this.skeletonQueue.Enqueue(skeleton.TrackingId);
                    }
                }

                
                IEnumerable<int> skeletonIds = 
                    from s in this.skeletonData
                    select s.TrackingId;

                // deactivate lost skeletons
                if (this.activatedSkeletons.Count > 0)
                {
                    List<int> keys = new List<int>(this.activatedSkeletons.Keys);
                    foreach (int key in keys)
                    {
                        //TrackedUser user = this.activatedSkeletons[key];
                        if (!skeletonIds.Contains(key))
                        {
                            Console.WriteLine(DateTime.Now + " Deactivate lost skeleton " + skeleton.TrackingId + ".");
                            this.DeactivateSkeleton(key);
                        }
                    }
                }

                // remove lost tracked skeletons
                if (this.trackedSkeletons.Count > 0)
                {
                    foreach (int key in this.trackedSkeletons.ToList())
                    {
                        if (!skeletonIds.Contains(key))
                        {
                            this.trackedSkeletons.Remove(key);
                        }
                    }
                }

                // cycle remaining tracked skeleton slots to search for new users
                while (this.skeletonQueue.Count > 0 && this.trackedSkeletons.Count < MaximumNumberOfTrackedSkeletons)
                {
                    int candidate = this.skeletonQueue.Dequeue();

                    // check if skeleton still exists and start tracking it
                    if(skeletonIds.Contains(candidate)) {
                        //Console.WriteLine(DateTime.Now + " Start tracking skeleton " + skeleton.TrackingId + ".");
                        this.trackedSkeletons.Add(candidate);
                    }
                }

                // choose skeletons for next frame
                switch (this.trackedSkeletons.Count)
                {
                    case 0: this.sensor.SkeletonStream.ChooseSkeletons();
                        break;
                    case 1: this.sensor.SkeletonStream.ChooseSkeletons(this.trackedSkeletons.ElementAt(0));
                        break;
                    case 2: this.sensor.SkeletonStream.ChooseSkeletons(this.trackedSkeletons.ElementAt(0), this.trackedSkeletons.ElementAt(1));
                        break;
                }
            }
        }

        /// <summary>
        ///     determine if skeleton does the defined activation gesture
        /// </summary>
        /// <param name="skeleton">skeleton to be checked</param>
        /// <returns>true if performing activation gesture</returns>
        private bool DoesActivationGesture(Skeleton skeleton)
        {
            // y axis points upwards, z is viewing axis of kinect, x goes to the left seen from the kinect
            bool active = false;
            if (
                (skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y + 0.2)
                && skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ElbowLeft].Position.Y + 0.2)
                )
                ||
                (skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y + 0.2)
                && skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ElbowRight].Position.Y + 0.2)
                )
                )
            {
                active = true;
            }

            return active;
        }

        /// <summary>
        ///     check, if skeleton shall remain active (based on lowering its hand for now)
        /// </summary>
        /// <param name="skeleton">skeleton to be checked</param>
        /// <returns>true if skeleton shall stay active</returns>
        private bool StayActive(Skeleton skeleton)
        {
            // y axis points upwards, z is viewing axis of kinect, x goes to the left seen from the kinect
            bool stayActive = false;
            if (
                (skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y - 0.06) &&
                    skeleton.Joints[JointType.WristLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y - 0.06)) ||
                (skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y - 0.06) &&
                    skeleton.Joints[JointType.WristRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y - 0.06))
                )
            {
                stayActive = true;
            }

            return stayActive;
        }

        /// <summary>
        ///     activate tracking for a skeleton after it did its activation gesture
        /// </summary>
        /// <param name="skeleton"></param>
        private void ActivateSkeleton(Skeleton skeleton)
        {
            bool leftHand = skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.HandRight].Position.Y;
            this.activatedSkeletons.Add(skeleton.TrackingId, new TrackedUser(skeleton) { TrackLeftHand = leftHand });
        }

        /// <summary>
        ///     deactivate a skeleton
        /// </summary>
        /// <param name="skeletonId"></param>
        private void DeactivateSkeleton(int skeletonId)
        {   
            TrackedUser user;
            this.activatedSkeletons.TryGetValue(skeletonId, out user);

            this.deactivatedSkeletons.Add(skeletonId, user);
            this.activatedSkeletons.Remove(skeletonId);
            this.trackedSkeletons.Remove(skeletonId);

            Thread thread = new Thread(delegate() {
                this.HandleDeactivatedSkeleton(skeletonId);
            });
            thread.Start();
        }

        /// <summary>
        ///     handle an deactivated skeleton (store, send data,...)
        /// </summary>
        /// <param name="skeletonId"></param>
        private void HandleDeactivatedSkeleton(int skeletonId)
        {
            TrackedUser user;
            this.deactivatedSkeletons.TryGetValue(skeletonId, out user);
            if (user != null)
            {
                if (user.Save()) // false if not worth saving (short data log)
                {
                    this.webClient.StoreData(user.getData());
                }
            }
            this.deactivatedSkeletons.Remove(skeletonId);
        }

    }
}
