﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Json;
using System.Text;
using System.Threading.Tasks;

namespace Tracker
{
    class WebClient
    {
        protected double serverClockLag = 0;
        protected LinkedList<double> serverClockLags = new LinkedList<double>();
        protected string url = "http://localhost/praktikum_teco_acc_auth/";

        public WebClient()
        {
            this.url = ConfigurationManager.AppSettings["ServerURL"];
        }

        /// <summary>
        ///     send data to server
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool StoreData(JsonObjectCollection data)
        {
            for (int i = 0; i < 10; i++)
            {
                this.Synchronize();
            }
            IEnumerable<double> recentLags = serverClockLags.Take(10);
            serverClockLag = recentLags.Average();
            
            data.Add(new JsonStringValue("estimatedServerClockLag", serverClockLag.ToString(CultureInfo.CreateSpecificCulture("en-GB"))));
            //TODO try catch for 404s
            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "?action=store");
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = data.ToString();
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/json";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            System.IO.Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return false;
        }

        /// <summary>
        ///     synchronize client clock with webserver's clock
        /// </summary>
        public void Synchronize()
        {
            double originalSent, sent, originalReceived, received;
            originalSent = 0; sent = 0; originalReceived = 0; received = 0;
            // TODO try catch for 404s
            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url + "?action=ptp");
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";
            // Create POST data and convert it to a byte array.
            originalSent = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
            string postData = "sent=" + originalSent;
            //postData += "&password=pass";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            System.IO.Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            received = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();


            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(String.Format(
                "Server error (HTTP {0}: {1}).",
                response.StatusCode,
                response.StatusDescription));
            JsonTextParser parser = new JsonTextParser();
            JsonObject obj = parser.Parse(responseFromServer);
            foreach (JsonObject field in obj as JsonObjectCollection)
            {
                string name = field.Name;
                string value = string.Empty;
                string type = field.GetValue().GetType().Name;

                // try to get value.
                switch (type)
                {
                    case "String":
                        value = (string)field.GetValue();
                        break;

                    case "Double":
                        value = field.GetValue().ToString();
                        break;

                    case "Boolean":
                        value = field.GetValue().ToString();
                        break;

                    default:
                        // in this sample we'll not parse nested arrays or objects.
                        throw new NotSupportedException();
                }
                switch (name)
                {
                    case "originalSent":
                        originalSent = Convert.ToDouble(value);
                        break;

                    case "received":
                        originalReceived = Convert.ToDouble(value);
                        break;

                    case "sent":
                        sent = Convert.ToDouble(value);
                        break;
                }
            }

            double lag = 1 / 2.0 * (received - originalReceived + originalSent - sent);
            serverClockLags.AddFirst(lag);
            serverClockLag = lag;
        }
    }
}
