\subsection{Tracking}
The first generation Kinect sensor supports simultaneous tracking of two skeletons at 30 fps. In total, six skeletons can be detected. The camera resolution is 640px x 480px. There also is a 1280px x 960px @ 15fps option for the rgb-stream, though the depth stream is only available at 640px x 480px @ 30fps and lower resolutions.

Tracking quality significantly decreases or is even impossible under strong sunlight. It also seems to be better in \emph{default} mode than in \emph{seated} mode, which partially relies on differential movement detection to separate the skeleton from the object it is sitting on. There is the possibility of fine grained smoothing control with five smoothing parameters. But altering those only had minimal effects on the matching results and sometimes caused problems with accurately detecting faster or irregular movements.

In fall 2013, the next generation of Kinect sensors will be available which use significantly higher video resolution and will therefore probably deliver much better tracking accuracy.

\subsection{Time Synchronization}
In this work, a time synchronization algorithm was implemented that estimates the clock offset by estimating the round-trip delay as follows, similar to the basic principle used in the precision time protocol:
\[
lag_{client} = \frac{1}{2} * (received_{client} - received_{server} + sent_{client} - sent_{server}) 
 \]
There was quite some jitter (about 200ms) due to software stacks and wireless network transmission, but averaging over ten consecutive synchronization requests lead to results within 30ms offset between the two clocks. This can be used to make brute force attacks harder, but does not provide security on its own.

Results may be improved with PHP version >= 5.4, as a more accurate timestamp for when request are received is available since that version (\$\_SERVER['REQUEST\_TIME\_FLOAT']). If available, the current implementation uses that one.

Sampling and synchronization can be performed in parallel if the estimated clock lag is only added as metadata to the samples' timestamp. Based on synchronized clocks, search windows for the matching process can be made smaller thus significantly improving performance compared to a full convolution of the pattern and the reference.

\subsection{Accessing Acceleration Sensors via JavaScript}

The possibility to access acceleration and rotation sensors via JavaScript using DeviceMotionEvents\cite{Block2012} is relatively new and therefore not yet widely supported. There also are significant differences from browser to browser and device to device.

As it is impossible to detect the devices orientation with the Kinect sensor, the best signal to continue with would be acceleration without earth gravitation.

Table \ref{tab:js_acceleration_support} shows current support for acceleration and orientation events on some platforms.

\begin{longtable}{>{\raggedright}p{3cm}|>{\raggedright}p{2cm}|>{\raggedright}p{3cm}|>{\raggedright}p{3cm}} %
\caption{Support of devicemotion and deviceorientation-events}\\ %
%Diese & erscheint nur & auf der ersten Seite\\ \hline\hline
%\endfirsthead
\textbf{Device} & \textbf{Browser} & \textbf{devicemotion} & \textbf{deviceorientation}\tabularnewline \hline %
\endhead %
%Die Fußzeilen & für & alle Seiten \\ \hline\hline
%\endfoot
%Nur die letzte
%\ endlastfoot
Acer W510, Windows 8 & Firefox 20.0.1 & yes (but acceleration without gravity and orientation always null) & yes (but no events fired)\tabularnewline
 & Opera 12.15 & no & no\tabularnewline
 & IE 10 & no & no\tabularnewline
  & Chrome 27 & yes (but no events fired) & yes (but only one intial event fired)\tabularnewline\hline
Samsung Galax S Advance + S3 Mini, Android 4.1.2 & Android Stock & yes (but acceleration without gravity and orientation always null) & yes\tabularnewline
 & Chrome & no & yes\tabularnewline
 & Firefox & yes & yes\tabularnewline
 & Opera & no & no\tabularnewline\hline
HTC Desire Z, Android 4.0.4 CM & Firefox & yes (but acceleration without gravity null) & yes\tabularnewline
 %\tabularnewline
\label{tab:js_acceleration_support} %
\end{longtable}

All browsers delivered samples with about 10Hz, but Firefox on Android delivered 70-80Hz. Though, when the devices changes its screen orientation, the event stream on Android with Firefox is interrupted up to 250ms.

The iPhone 4 worked fine too, except for using milliseconds for event-timestamps instead of microseconds like Android does. The event frequency was about 35Hz.

One problem that occurred during the development of this prototype were value overflows at around 3.5g with the Samsung Galaxy S Advance that lead to a sudden jump from positive to negative values. Upon detecting this sudden extreme jump in acceleration, the last plausible value is repeated (simple hold) until the actual signal returns to normal values.

\subsection{Measuring acceleration with the Kinect}
Position samples delivered by the Kinect are rather noisy. If velocities and acceleration are derived without any smoothing, no waving movement is obviously visible in figure \ref{fig:acceleration_no_smoothing}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.9\linewidth]{../media/20130611_kinect3_acceleration}
\caption{Acceleration derived from raw position samples.}
\label{fig:acceleration_no_smoothing}
\end{figure}
After applying a irregular time series exponential average with a tau = 45ms, the velocities and accelerations clearly show the underlying waving movement in figure \ref{fig:acceleration_with_smoothing}. With this parametrization the amplitudes of acceleration derived from the Kinect sensor's samples and the smartphone's accelerometer match roughly.
\begin{figure}[htb]
\centering
\includegraphics[width=0.9\linewidth]{../media/20130611_kinect3_x_acceleration_velocity.png}
\caption{Acceleration and velocities along the x-axis derived from smoothed position samples.}
\label{fig:acceleration_with_smoothing}
\end{figure}

But one has to be careful, as smoothing may affect the amplitudes of the signal (the positions of features remain unchanged). For tau = 45ms, scaling the acceleration by a factor of 1.4 afterwards compensates for this effect. If stronger smoothing is necessary, the smoothing parameters that can be configured for the Kinect sensor seam to have less of an effect on the amplitude, as limits for deviations from the actually tracked position can be set. 

\subsection{Matching Tracked and Measured Movements}
During this work, different metrics and correlations where implemented. Though, besides some difference in computation time (with the Pearson Correlation being one of the fastest ones) and absolute values, they all result in pretty similar results. Out of the implemented ones, the Pearson Squared Correlation and the Mirrored Normalized Mean Squared Error are the most promising ones. Moving a 30 second pattern across 35-40 seconds of reference using steps of 33ms and computing the metric at every position takes about 4 seconds per dataset pair on a 2,2 Ghz Intel Penryn core using the Pearson Squared Correlation. Using synchronized clocks, it suffices to search the best match within one or two seconds instead of 30, which results in computation times within tenths of a seconds.

One important aspect is rotation invariance, as the devices rotation cannot be tracked reliably with the first generation Kinect. Rotation invariance can be achieved by only using rotation invariant features or signals. The easiest one is using the norm of the acceleration vector. Another way is using the value of samples projected on the movement's principal component, which should roughly align for data sampled with the smartphone and the Kinect. It yielded significantly better results than the norm of the acceleration vector. Probably, by picking the main movement direction as a feature, measurement errors in other directions had less of an impact on the result.

The strongest attacker tested was an imitating attacker. Unfortunately, a clear classification is sometimes hard to make, a classical classification problem is present. Figure \ref{fig:scatterplot_classification_problem} shows results from some test data that does not allow clear classification. Additionally, there seams to be a correlation between matching scores and movement entropy (lower scores for higher movement entropy probably caused by sensor noise or insufficient modeling of expected accelerometer readings).
%
\fboxsep=0.2mm%padding thickness
\fboxrule=0.2pt%border thickness
\begin{figure} %
\centering
\fbox{\includegraphics[width=1.0\linewidth]{../media/scatterplot_20130807-datasets}}
\caption{Scatterplot showing actual dataset pairs in green, imitated and cross-paired pairs in red. The left edge means no similarity, the right edge a perfect match.}
\label{fig:scatterplot_classification_problem}
\end{figure}

