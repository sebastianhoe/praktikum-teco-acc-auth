﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tracker
{
    /// <summary>
    ///     cycles through detected sceletons checking for the activation gesture and tracks activated skeletons
    /// </summary>
    class UserTracker
    {
        private KinectSensor sensor;
        private WebClient webClient = new WebClient();

        private BodyFrameReader bodyReader;
        /// <summary>
        ///     array holding all skeleton data
        /// </summary>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/jj131025"/>
        private Body[] skeletonData;

        /// <summary>
        ///     maximum number of simultaneously tracked skeletons, determined by the kinect sdk
        /// </summary>
        private const int MaximumNumberOfTrackedSkeletons = 2;

        /// <summary>
        ///     array holding ids of actively tracked skeletons
        /// </summary>
        private List<ulong> trackedSkeletons = new List<ulong>(MaximumNumberOfTrackedSkeletons);

        /// <summary>
        ///     array holding ids of deactivated skeletons (tracking finished)
        /// </summary>
        private Dictionary<ulong, TrackedUser> deactivatedSkeletons = new Dictionary<ulong, TrackedUser>();

        /// <summary>
        ///     array holding ids of activated skeletons (by activation gesture)
        /// </summary>
        private Dictionary<ulong, TrackedUser> activatedSkeletons = new Dictionary<ulong, TrackedUser>(MaximumNumberOfTrackedSkeletons);
         

        /// <summary>
        ///     queue of ids of skeletons not tracked, waiting to be checked for activation gesture
        /// </summary>
        private Queue<ulong> skeletonQueue = new Queue<ulong>();

        public UserTracker()
        {
        }

        /// <summary>
        ///     injector for the used kinect sensor
        /// </summary>
        public KinectSensor Sensor
        {
            get
            {
                return sensor;
            }
            set
            {
                // logic for changed sensor here
                sensor = value;
                this.skeletonData = new Body[sensor.BodyFrameSource.BodyCount];
                //if (!sensor.SkeletonStream.AppChoosesSkeletons)
                //{
                //    sensor.SkeletonStream.AppChoosesSkeletons = true;
                //}
                //sensor.SkeletonFrameReady += this.SkeletonFrameReady;
                this.bodyReader = this.sensor.BodyFrameSource.OpenReader();
                this.bodyReader.FrameArrived += SkeletonFrameReady;
            }
        }

        /// <summary>
        ///     list of actively tracked skeleton ids
        /// </summary>
        public List<ulong> ActiveSkeletonIds
        {
            get
            {
                return activatedSkeletons.Keys.ToList();
            }
        }
        
        /// <summary>
        ///     receive and use skeleton data from kinect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">sekeleton frame data from kinect</param>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/jj131025"/>
        private void SkeletonFrameReady(object sender, BodyFrameArrivedEventArgs e)
        {
            long timestamp = 0;
            bool skeletonFrameSet = false;

            using (BodyFrame skeletonFrame = e.FrameReference.AcquireFrame()) // Open the Skeleton frame
            {
                if (skeletonFrame != null && this.skeletonData != null) // check that a frame is available
                {
                    skeletonFrame.GetAndRefreshBodyData(this.skeletonData); // get the skeletal information in this frame
                    skeletonFrameSet = true;
                    timestamp = skeletonFrame.RelativeTime / 10000;
                }
            }

            if (!skeletonFrameSet)
            {
                return;
            }

            foreach (Body skeleton in this.skeletonData)
            {

                if (skeleton.IsTracked)
                {
                    if (this.activatedSkeletons.ContainsKey(skeleton.TrackingId))
                    {
                        if (!this.StayActive(skeleton))
                        {
                            this.DeactivateSkeleton(skeleton.TrackingId);
                        }
                        else
                        {
                            TrackedUser user;
                            this.activatedSkeletons.TryGetValue(skeleton.TrackingId, out user);
                            user.AddSample(skeleton, timestamp);
                        }
                        // TODO limit tracking time
                    }
                    else
                    {
                        // check for activation gesture
                        if (this.DoesActivationGesture(skeleton))
                        {
                            this.ActivateSkeleton(skeleton);
                        }
                        else
                        {
                            // if not doing activation gesture, remove from list of tracked skeletons
                            // so that other skeletons can be tracked
                            //Console.WriteLine(DateTime.Now + " Stop tracking skeleton " + skeleton.TrackingId + " as of not doing activation gesture.");
                            this.trackedSkeletons.Remove(skeleton.TrackingId);
                        }
                    }
                }
                //else if (skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                //{
                //    if (!this.skeletonQueue.Contains(skeleton.TrackingId))
                //    {
                //        //Console.WriteLine(DateTime.Now + " Enqueue skeleton " + skeleton.TrackingId + " to waiting list.");
                //        this.skeletonQueue.Enqueue(skeleton.TrackingId);
                //    }
                //}

                
                IEnumerable<ulong> skeletonIds = 
                    from s in this.skeletonData
                    select s.TrackingId;

                // deactivate lost skeletons
                if (this.activatedSkeletons.Count > 0)
                {
                    List<ulong> keys = new List<ulong>(this.activatedSkeletons.Keys);
                    foreach (ulong key in keys)
                    {
                        //TrackedUser user = this.activatedSkeletons[key];
                        if (!skeletonIds.Contains(key))
                        {
                            Console.WriteLine(DateTime.Now + " Deactivate lost skeleton " + skeleton.TrackingId + ".");
                            this.DeactivateSkeleton(key);
                        }
                    }
                }

                // remove lost tracked skeletons
                if (this.trackedSkeletons.Count > 0)
                {
                    foreach (ulong key in this.trackedSkeletons.ToList())
                    {
                        if (!skeletonIds.Contains(key))
                        {
                            this.trackedSkeletons.Remove(key);
                        }
                    }
                }

                // cycle remaining tracked skeleton slots to search for new users
                while (this.skeletonQueue.Count > 0 && this.trackedSkeletons.Count < MaximumNumberOfTrackedSkeletons)
                {
                    ulong candidate = this.skeletonQueue.Dequeue();

                    // check if skeleton still exists and start tracking it
                    if(skeletonIds.Contains(candidate)) {
                        //Console.WriteLine(DateTime.Now + " Start tracking skeleton " + skeleton.TrackingId + ".");
                        this.trackedSkeletons.Add(candidate);
                    }
                }

                //// choose skeletons for next frame
                //switch (this.trackedSkeletons.Count)
                //{
                //    case 0: this.sensor.SkeletonStream.ChooseSkeletons();
                //        break;
                //    case 1: this.sensor.SkeletonStream.ChooseSkeletons(this.trackedSkeletons.ElementAt(0));
                //        break;
                //    case 2: this.sensor.SkeletonStream.ChooseSkeletons(this.trackedSkeletons.ElementAt(0), this.trackedSkeletons.ElementAt(1));
                //        break;
                //}
            }
        }

        /// <summary>
        ///     determine if skeleton does the defined activation gesture
        /// </summary>
        /// <param name="skeleton">skeleton to be checked</param>
        /// <returns>true if performing activation gesture</returns>
        private bool DoesActivationGesture(Body skeleton)
        {
            // y axis points upwards, z is viewing axis of kinect, x goes to the left seen from the kinect
            bool active = false;
            if (
                (skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y + 0.2)
                && skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ElbowLeft].Position.Y + 0.2)
                )
                ||
                (skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y + 0.2)
                && skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ElbowRight].Position.Y + 0.2)
                )
                )
            {
                active = true;
            }

            return active;
        }

        /// <summary>
        ///     check, if skeleton shall remain active (based on lowering its hand for now)
        /// </summary>
        /// <param name="skeleton">skeleton to be checked</param>
        /// <returns>true if skeleton shall stay active</returns>
        private bool StayActive(Body skeleton)
        {
            // y axis points upwards, z is viewing axis of kinect, x goes to the left seen from the kinect
            bool stayActive = false;
            if (
                (skeleton.Joints[JointType.HandLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y - 0.06) &&
                    skeleton.Joints[JointType.WristLeft].Position.Y > (skeleton.Joints[JointType.ShoulderLeft].Position.Y - 0.06)) ||
                (skeleton.Joints[JointType.HandRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y - 0.06) &&
                    skeleton.Joints[JointType.WristRight].Position.Y > (skeleton.Joints[JointType.ShoulderRight].Position.Y - 0.06))
                )
            {
                stayActive = true;
            }

            return stayActive;
        }

        /// <summary>
        ///     activate tracking for a skeleton after it did its activation gesture
        /// </summary>
        /// <param name="skeleton"></param>
        private void ActivateSkeleton(Body skeleton)
        {
            bool leftHand = skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.HandRight].Position.Y;
            this.activatedSkeletons.Add(skeleton.TrackingId, new TrackedUser(skeleton) { TrackLeftHand = leftHand });
        }

        /// <summary>
        ///     deactivate a skeleton
        /// </summary>
        /// <param name="skeletonId"></param>
        private void DeactivateSkeleton(ulong skeletonId)
        {   
            TrackedUser user;
            this.activatedSkeletons.TryGetValue(skeletonId, out user);

            this.deactivatedSkeletons.Add(skeletonId, user);
            this.activatedSkeletons.Remove(skeletonId);
            this.trackedSkeletons.Remove(skeletonId);

            Thread thread = new Thread(delegate() {
                this.HandleDeactivatedSkeleton(skeletonId);
            });
            thread.Start();
        }

        /// <summary>
        ///     handle an deactivated skeleton (store, send data,...)
        /// </summary>
        /// <param name="skeletonId"></param>
        private void HandleDeactivatedSkeleton(ulong skeletonId)
        {
            TrackedUser user;
            this.deactivatedSkeletons.TryGetValue(skeletonId, out user);
            if (user != null)
            {
                if (user.Save()) // false if not worth saving (short data log)
                {
                    this.webClient.StoreData(user.getData());
                }
            }
            this.deactivatedSkeletons.Remove(skeletonId);
        }

    }
}
