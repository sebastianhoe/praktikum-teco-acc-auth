﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using System.Net.Json;

namespace Tracker
{
    /// <summary>
    /// class to hold tracked user and its tracked data.
    /// </summary>
    class TrackedUser
    {
        public struct Sample
        {
            /// <summary>
            ///     position of a skeleton joint
            /// </summary>
            public CameraSpacePoint position;

            /// <summary>
            ///     timestamp in ms since intialization of kinect sensor
            /// </summary>
            public long timestamp;
        }

        private List<Sample> log = new List<Sample>();

        private long timestampBase = 0;

        private Body skeleton;

        public TrackedUser(Body skeleton)
        {
            this.skeleton = skeleton;
        }

        public List<Sample> Log
        {
            get
            {
                return log.ToList();
            }
        }

        /// <summary>
        ///     add sample to tracked user
        /// </summary>
        /// <param name="skeleton">skeleton containing joint positions</param>
        /// <param name="timestamp">timestamp of event</param>
        public void AddSample(Body skeleton, long timestamp)
        {
            // store timestamp base, if not already done
            // timestamps normally count from the initialization of the kinect sensor, therefore we need to compute the offeset to current time
            if (this.timestampBase == 0)
            {
                Console.WriteLine(DateTime.Now + " Timestamp received: " + timestamp);
                this.timestampBase = (long) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
                Console.WriteLine(DateTime.Now + " New timebase for skeleton: " + this.timestampBase);
                this.timestampBase -= timestamp;
                Console.WriteLine(DateTime.Now + " Updated timebase for skeleton: " + this.timestampBase);

            }

            // create sample
            Sample sample = new Sample {
                timestamp = this.timestampBase + timestamp
            };
            
            // get position of tracked hand (wrist tends to jump up and down a bit too much)
            if (this.TrackLeftHand)
            {
                sample.position = skeleton.Joints[JointType.HandLeft].Position;
            }
            else
            {
                sample.position = skeleton.Joints[JointType.HandRight].Position;
            }
            this.log.Add(sample);
        }

        /// <summary>
        ///     save tracked user if it contains enough samples (to avoid to many fake users from wrong tracking)
        /// </summary>
        /// <returns>true on success</returns>
        public bool Save()
        {
            if(this.log.Count < 150) {
                return false;
            }

            //Console.WriteLine("Generated object:");
            JsonUtility.GenerateIndentedJsonText = true;
            //Console.WriteLine(collection);

            System.IO.StreamWriter file = new System.IO.StreamWriter(DateTime.UtcNow.ToString("yyyyMMdd_HHmmss") + "_kinect.json");
            file.WriteLine(this.getData());
            file.Close();

            //Console.WriteLine();
            //Console.ReadLine();

            return true;  
        }

        /// <summary>
        ///     get this user's data as JsonObjectCollection
        /// </summary>
        /// <returns>this user's data (samples, etc.)</returns>
        public JsonObjectCollection getData()
        {
            JsonObjectCollection collection = new JsonObjectCollection();
            collection.Add(new JsonStringValue("comment", ""));
            collection.Add(new JsonNumericValue("trackingId", this.skeleton.TrackingId));
            collection.Add(new JsonStringValue("name", "Kinect"));
            collection.Add(new JsonNumericValue("timestamp", (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds));

            List<JsonObjectCollection> positions = new List<JsonObjectCollection>(this.log.Count);
            foreach (Sample sample in this.log)
            {
                JsonObjectCollection jsonSample = new JsonObjectCollection();
                jsonSample.Add(new JsonNumericValue("ts", sample.timestamp));
                jsonSample.Add(new JsonNumericValue("x", sample.position.X));
                jsonSample.Add(new JsonNumericValue("y", sample.position.Y));
                jsonSample.Add(new JsonNumericValue("z", sample.position.Z));
                positions.Add(jsonSample);
            }
            collection.Add(new JsonArrayCollection("pos", positions));

            return collection;
        }

        public bool TrackLeftHand = false;
    }
}
