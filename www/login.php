<?php
if ( !defined('CP') ) { die("Invalid access.");}
/* 
 * Captive Portal login page with user guidance. 
 * */

// default time to sample for now
$samplingTime = isset($config['sampling']['time']) ? $config['sampling']['time'] : 20000;

if(isset($_POST['auth_user'])) {
	$_SESSION['auth_user'] = $_POST['auth_user'];
} else if(isset($_SESSION['auth_user'])) {
	$_POST['auth_user'] = $_SESSION['auth_user'];
}

// alternative way to get data sent via post, in case of problems with json-post-data.
function getRealPOST() {
	$pairs = explode("&", file_get_contents("php://input"));
	$vars = array();
	foreach ($pairs as $pair) {
		$nv = explode("=", $pair);
		$name = urldecode($nv[0]);
		$value = urldecode($nv[1]);
		$vars[$name] = $value;
	}
	return $vars;
}

// if a sample is submitted, store it
if(isset($_POST['sample'])) {
	$sample = $_POST['sample'];
	
	$data = new stdClass();
	$data->stored = false;
	$data->message = 'Not samples found.';
	
	$filename = date('Ymd_His').'_'.session_id().'.json';
	$fp = fopen($dataDirectory.$filename, 'w');
	$_SESSION['filename'] = $filename;
	
	$trackingId = md5('ageahhwe'.session_id());
	if(is_object($sample)) {
		$sample->trackingId = $trackingId;
	} else {
		$sample['trackingId'] = $trackingId;
	}

	fwrite($fp, json_encode($sample));
	fclose($fp);
	
	$data->stored = true;
	$data->message = 'Samples have been stored. Use verify-service to check authentication.';
	
	echo json_encode($data);
	exit;
} 

?><!DOCTYPE html>
<html>
<head>
<!-- define viewport for optimized viewing on smartphones etc. -->
<meta name="viewport" content="width=480">
<link rel="stylesheet" type="text/css" href="<?=FILE_PREFIX?>style.css">
<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.min.js"></script>

<script type="text/javascript">
	var console = window.console || { log: function() {} };

	/* estimate time offset between server and local device */
	<?php $serverTime = microtime(true)*1000;?>
	var localTimestamp = new Date().getTime();
	var serverClockLag = <?=$serverTime?> - localTimestamp; 
	var serverClockLags = new Array();

	/* container for acceleration samples */
	var accData = new Array();
	var accGData = new Array();
	var oriData = new Array();
	var sampling = false;
	function addAccSample(sample) {
		if(sample == null) { sample = {};}
		if(!sample.ts) { sample.ts = new Date().getTime();}
		accData.push(sample);
	}
	function addAccGSample(sample) {
		if(sample == null) { sample = {};}
		if(!sample.ts) { sample.ts = new Date().getTime();}
		accGData.push(sample);
	}
	function addOriSample(sample) {
		if(sample == null) { sample = {};}
		if(!sample.ts) { sample.ts = new Date().getTime();}
		oriData.push(sample);
	}
	function clearSamples() {
		accData = new Array();
		accGData = new Array();
		oriData = new Array();
	}

	/* helper to update displayed message */
	function updateMessage(text) {
		$('#message').html(text);
	}

	// start authentication process
	var samplingTimeouts = new Array();
	$(function() {
		$("#localTimestamp").html(localTimestamp);
		
		$("#auth_submit").click(function() {
			if(sampling) { 
				if (confirm('Abort Log-In Process?')) {
					abortSampling();
				}
				return false; 
			}
			sampling = true;
			clearSamples();
			$("#auth_submit").fadeTo('slow', 0.4);
			updateMessage('Sampling...');
			try {
				navigator.vibrate(50);
			} catch(e) {}
			var samplingTime = $('#auth_sampling_time').val();
			for(var i = 0; i <  samplingTime; i = i+1000) {
				(function(index) {
			        samplingTimeouts.push(setTimeout(function() { updateMessage('Sampling...' + (samplingTime - index)); $('#auth_submit').val(((samplingTime - index)/1000.0).toFixed(0) + ' sec'); }, index));
			    })(i);
			}
			samplingTimeouts.push(setTimeout(function() { $('#auth_submit').val('LOG IN'); sendData(); }, samplingTime));

			return false;
		});
	});

	// abort sampling / authentication process
	abortSampling = function() {
		sampling = false;
		clearSamples();
		for(var i = 0; i < samplingTimeouts.length; i++) {
			clearTimeout(samplingTimeouts[i]);
		}
		$("#auth_submit").fadeTo('slow', 1.0);
		$('#auth_submit').val('LOG IN');
		updateMessage('Sampling aborted.');
	};

	// send sampled data to server at the end of the sampling period
	sendData = function() {
		sampling = false;
		
		updateMessage("Sending...");
		
		try {
			setTimeout("navigator.vibrate(200);", 10);
			setTimeout("navigator.vibrate(300);", 400);
			setTimeout("navigator.vibrate(200);", 800);
		} catch(e) {}
		
		var data = {
				sample: {
					name: $('#auth_user').val(),
					comment: $('#auth_comment').val(),
					estimatedServerClockLag: serverClockLag,
					timestamp: (new Date().getTime()),
					acc: accData,
					accG: accGData,
					ori: oriData
				}
			};
		updateMessage("Sending...data prepared...");
		//console.log(data);
		
		$.ajax({
			type: "POST",
			url: "<?=FILE_PREFIX?>index.php?action=login",
			//contentType:'multipart/form-data; charset=UTF-8',
			async: false,
			data: data,
			success: function(data, textStatus, jqXHR) {
				updateMessage("Success. Verifying...");
				
				setTimeout(function() { tryAuthorization(); }, 2000);
			},
			error: function(jqXHR, testStatus, errorThrown) {
				updateMessage("Error.");
			},
			complete: function(jqXHR, textStatus) {
				try {
					navigator.vibrate(100);
				} catch(e) {}
				
				clearSamples();	
			}
		});

		return false;
	};

	// check at regular interval, if user has been authorized 
	// (it may take some time for the samples from the kinect to arrive at the server)
	var authorizationIntervalTimer = null; 
	tryAuthorization = function() {
		authorize();
		authorizationIntervalTimer = setInterval(function() { authorize(); }, 5000);
		setTimeout(function() { stopAuthorizationTries(); }, 20000);
	}

	stopAuthorizationTries = function() {
		 clearInterval(authorizationIntervalTimer);
	}

	authorize = function() {
		updateMessage("Authorizing...");
		
		$.ajax({
			type: "POST",
			url: "<?=FILE_PREFIX?>index.php?action=authorize",
			//contentType:'multipart/form-data; charset=UTF-8',
			async: false,
			dataType: "json",
			success: function(data, textStatus, jqXHR) {
				updateMessage(data.message + '(' + data.score + ')');

				if(data.accepted) {
					stopAuthorizationTries();
					$('body').css('background-color', '#00ff00');
				} else {
					$('body').css('background-color', '#ff0000');
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				updateMessage("Error during authorization. " + textStatus);
			},
			complete: function(jqXHR, textStatus) {
				try {
					navigator.vibrate(50);
				} catch(e) {}
			}
		});

		return false;
	};

	// clock synchronization ajax calls
	synchronizeClocks = function() {
		updateMessage("Synching...");

		var data = {sent: (new Date().getTime())};
		$.ajax({
			type: "POST",
			url: "<?=FILE_PREFIX?>index.php?action=ptp",
			//contentType:'multipart/form-data; charset=UTF-8',
			async: true,
			global: false, // prevent ajax events
			data: data,
			dataType: "json",
			success: function(data, textStatus, jqXHR) {
				var received = (new Date().getTime());
				var offsetInMs = 1/2 * (received - data.received + data.originalSent - data.sent);
				serverClockLags.push(offsetInMs);
				

				// compute average of last 10 computed lags
				var values = serverClockLags.slice(serverClockLags.length - 10);
				var sum = values.reduce(function(a, b) { return a + b });
				var avg = sum / values.length;
				serverClockLag = avg;

				$('#serverClockLags').html(serverClockLags.slice(serverClockLags.length - 10).join(", ") + "<br/> avg: " + avg);
				updateMessage("Server clock lag: " + (serverClockLag/1000.0).toFixed(3) + "s");
			},
			error: function(jqXHR, testStatus, errorThrown) {
				updateMessage("Synchronization Error.");
			}
		});
		return false;
	};
	</script>

<script type="text/javascript">
	// show data received from sensors
	$(function() { // check when document is loaded to be able to access elements
		if (window.DeviceMotionEvent) { console.log("DeviceMotionEvent supported");} 
		if (window.DeviceOrientationEvent) { console.log("DeviceOrientationEvent supported");} 

		if (window.DeviceOrientationEvent) {
			document.getElementById("doEvent").innerHTML = "Yes.";
			window.addEventListener('deviceorientation', function(eventData) {
				if(sampling) {
					//console.log(eventData);
					var sample = {
						alpha : eventData.alpha,
						beta : eventData.beta,
						gamma : eventData.gamma,
						absolute: eventData.absolute 
					};
					addOriSample(sample);
				}
				//document.getElementById("moOrientation").innerHTML = "[" + Math.round(eventData.alpha) + ", " + Math.round(eventData.beta) + ", " + Math.round(eventData.gamma) + "]";
			}, true);
		} else {
		  document.getElementById("doEvent").innerHTML = "Not supported on your device.";
		}
		
		if (window.DeviceMotionEvent) {
			document.getElementById("dmEvent").innerHTML = "Yes.";
			window.addEventListener('devicemotion', deviceMotionHandler, false);
		} else {
		  document.getElementById("dmEvent").innerHTML = "Not supported on your device.";
		}
	});

	// convert object to string (serialize)
	function printObject(o) {
	  var out = '';
	  for (var p in o) {
		  if(typeof(o[p]) === 'object') {
			  out += p + ': ' + printObject(o[p]) + '\n';
		  } else {
	    	out += p + ': ' + o[p] + '\n';
		  }
	  }
	  return out;
	}

	// handle event from motion sensor
	function deviceMotionHandler(eventData) {
	  var acceleration = eventData.accelerationIncludingGravity;
	  var accelerationNoG = eventData.acceleration; // tends to be null on many devices :(

	  //document.getElementById("moAccel").innerHTML = acceleration.x + ", "+acceleration.y+", "+acceleration.z;
		
		if(sampling) {
			// firefox for android makes it necessary to cherry pick required values
			var ts;
			if(eventData.timeStamp) {
				// android and ios use different units
				if(eventData.timeStamp > 10000000000000) {
					ts = eventData.timeStamp / 1000;
				} else {
					ts = eventData.timeStamp;
				}
			} else {
				ts = new Date(). getTime();
			}
			//ts = ts - serverClockLag; // use local clock to allow asynchronous synchronization
			var sample = {
					x : acceleration.x,
					y : acceleration.y,
					z : acceleration.z,
					ts: ts
				};
			 
			addAccGSample(sample);
			if(accelerationNoG != null) {
				sample = {
						x : accelerationNoG.x,
						y : accelerationNoG.y,
						z : accelerationNoG.z,
						ts: ts
					};

			    
				addAccSample(sample);
			}		
		}
	    document.getElementById("moCalcInterval").innerHTML = eventData.interval;
	}

	// integrate button to initiate synchronization manually
	$(function() {
		$("#button_synchronize").click(function (event) {
			event.preventDefault();
			for(var i = 0; i <  10; i = i+1) {
				(function(index) {
			        setTimeout(function() { synchronizeClocks(); }, i * 200);
			    })(i);
			}
		});

		setTimeout(function() {
			for(var i = 0; i <  10; i = i+1) {
				(function(index) {
			        setTimeout(function() { synchronizeClocks(); }, i * 200);
			    })(i);
			}
		}, 1000);
	});

	</script>
</head>

<body class="login">

<!-- rotating text and acc info -->
<div style="padding: 0px 10px;">
	<h2>WiFi Login</h2>
	<p>Please use Firefox and deactivate screen rotation.</p>
</div>
<img src="<?=FILE_PREFIX.'steps.png'?>" alt="1. Click 'Login', 2. Rise your hand, 3. Wave until smartphones vibrates, 4. Lower your hand and check status." />
<div style="padding: 0px 10px;">
	<!-- new login form -->
	<form method="post" action="#">
	<input name="start_authentication"
		id="auth_submit" type="submit" value="LOG IN">
	<div id="messageBox"><span id="message"></span>&nbsp;</div> 
	<table>
		<tr>
			<td>Name:</td>
			<td><input name="auth_user" id="auth_user" type="text"
				value="<?php echo (isset($_POST['auth_user']) ? $_POST['auth_user'] : ''); ?>"></td>
		</tr>
		<tr>
			<td>Comment:</td>
			<td><input name="auth_comment" id="auth_comment" type="text"
				value="<?php echo (isset($_POST['auth_comment']) ? $_POST['auth_comment'] : ''); ?>"></td>
		</tr>
		<tr>
			<td>Sampling Time:</td>
			<td><input name="auth_sampling_time" id="auth_sampling_time"
				type="text"
				value="<?php echo (isset($_POST['auth_sampling_time']) ? $_POST['auth_sampling_time'] : $samplingTime); ?>"></td>
		</tr>
	</table>
	</form>
	
	<input
		type="button" id="button_synchronize" name="synchronize"
		value="Synchronize" />
	
		
	<table id="sensorStatus">
		<tr>
			<td>Server Time:</td>
			<td><?=$serverTime?></td>
		</tr>
		<tr>
			<td>Local Time:</td>
			<td id="localTimestamp"></td>
		</tr>
		<tr>
			<td>Motion Event Supported:</td>
			<td id="dmEvent"></td>
		</tr>
		<tr>
			<td>Orientation Event Supported:</td>
			<td id="doEvent"></td>
		</tr>
		<tr>
			<td>Orientation:</td>
			<td id="moOrientation"></td>
		</tr>
		<tr>
			<td>ACC (including gravity):</td>
			<td id="moAccel"></td>
		</tr>
		<tr>
			<td>ACC:</td>
			<td id="moAccelNoG"></td>
		</tr>
		<tr>
			<td>Interval:</td>
			<td id="moCalcInterval"></td>
		</tr>
	</table>
	<div id="serverClockLags"></div>
</div>
	
<!-- login form required by pfsense -->
<div style="display: none">
<form method="post" action="$PORTAL_ACTION$"><input name="auth_user"
	type="text"> <input name="auth_pass" type="password"> <input
	name="auth_voucher" type="text"> <input name="redirurl" type="hidden"
	value="$PORTAL_REDIRURL$"> <input name="accept" type="submit"
	value="Continue"></form>
</div>
</body>
</html>
