<?php
// basic definitions
define('CP', true);
define('DEBUG', true);
date_default_timezone_set('UTC');

// shorten precision time protocol evaluation
switch(@$_GET['action']) { // if time synchronization request, immediatly continue with it to avoid any lag
	case 'ptp':
		include 'ptp.php';
		break;
}

// start session and send content type header before any output
session_start();
header( 'content-type: text/html; charset=utf-8' );

$config = parse_ini_file('config.ini', true);
$dataDirectory = isset($config['sampling']['directory']) ? $config['sampling']['directory'] : 'data/';

// load libraries
if(!class_exists('Dataset')) {
	include 'Dataset.php';
}
if(!class_exists('Matcher')) {
	include 'Matcher.php';
}
if(!class_exists('SPL')) {
	include 'SignalProcessingLibrary.php';
}

Dataset::setDataDirectory($dataDirectory);

// prefix for publicly accessible files given by pfsense
if(!defined('FILE_PREFIX')) {
	define('FILE_PREFIX', strpos(__FILE__, 'captivportal-') === false ? '' : 'captivportal-');
}

// continue depending on desired/selected action
switch(@$_GET['action']) {
	case 'webcamrecording': 
		include 'webcam_recording.php';
		break;
	case 'store':
		include 'store.php';
		break;
	case 'analysis_service':
		include 'analysis_service.php';
		break;
	case 'analysis_ui':
		include 'analysis_ui.php';
		break;
	case 'login':
		include 'login.php';
		break;
	case 'authorize':
		include 'authorize.php';
		break;
	default:
		// menu/entry page shown as default in prototype
		?>
		<html>
		<head>
			<meta name="viewport" content="width=480">
			<link rel="stylesheet" type="text/css" href="style.css">
		</head>
		<body>
		<div style="padding: 0px 10px;">
			<h1>Acc Authentication Within a Visual Context</h1>
			<p>by Sebastian H&ouml;ninger 2013</p>
			<h2><a href="?action=login">Login Page</a></h2>
			<!-- <h2><a href="?action=webcamrecording">Webcam Recording</a></h2>-->
			<h2><a href="?action=analysis_ui">Analysis</a></h2>
			
			<h1>Info</h1>
			<p>Server IP: <?=getHostByName(getHostName())?></p>
		</div>
		</body>
		</html>
		<?php 
		break;	
}
?>