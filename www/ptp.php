<?php
/*
 * Implementation of the server side of the precision time protocol for clock
 * synchronization.
 * Author: S.Höninger 2013
 */

// determine timestamp of receiving the client's message
$received = round(microtime(true)*1000);
if(isset($_SERVER['REQUEST_TIME_FLOAT'])) {
	$received = $_SERVER['REQUEST_TIME_FLOAT'] * 1000;
}

if ( !defined('CP') ) {die("Invalid access.");}

// prepare server's response
$data = new stdClass();
$data->received = $received;
$data->originalSent = floatval($_POST['sent']);
$data->sent = round(microtime(true)*1000);

// send response
echo json_encode($data);

// exit to stop code execution immediately
exit;
?>