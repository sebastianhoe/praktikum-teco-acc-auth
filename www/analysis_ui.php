<?php
/**
 * The analysis-user-interface is used to display stored datasets and show graphs of them.
 */ 
if ( !defined('CP') ) { die("Invalid access.");}

$valueTypes = array('x', 'y', 'z', 'x+G', 'y+G', 'z+G', 'alpha', 'beta', 'gamma', 'xpos', 'ypos', 'zpos', 'acc_norm', 'pc_proj_norm', 'pc_proj_value', 'accG_norm', 'cema', 'matching');
$valueDataSource = array('x' => array('acc', 'pos'), 'y' => array('acc', 'pos'), 'z' => array('acc', 'pos'),
						'x+G' => 'accG', 'y+G' => 'accG', 'z+G' => 'accG',
						'alpha' => 'ori', 'beta' => 'ori', 'gamma' => 'ori',
						'accNorm' => 'acc', 'pc_proj_norm' => array('acc', 'pos'), 'pc_proj_value' => array('acc', 'pos'), 'accG_norm' => 'accG',
						'xpos' => 'pos', 'ypos' => 'pos', 'zpos' => 'pos')

?>
<html>
<head>
	<title>Analysis</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.min.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.ajaxq.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.flot.min.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.flot.canvas.min.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.flot.legendoncanvas.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.flot.selection.min.js"></script>
	<script type="text/javascript" src="js/<?=FILE_PREFIX?>jquery.flot.navigate.min.js"></script>
	<script type="text/javascript">
		var data = [];
		var displayedData = [];

		// show running state of ajax requests
		var originalPageTitle = '';
		jQuery(document).ready(function($) { 
			$('body').append('<div id="ajaxLoading"><p>Computing...</p></div>');
			originalPageTitle = $(document).attr('title');
			$('#ajaxLoading').css({display:"none",margin:"0",padding:"20",border:"1px solid #ccc",background: "#fff",position:"absolute",position:"fixed",left:"10",top:"10",width:"auto"});
		});
		var ajaxLoaderTimeout;
		var ajaxLoaderSequenceTimeout;
		var ajaxLoaderSequenceCounter = 0;
		jQuery(document).ajaxStart(function($){
				ajaxLoaderTimeout = setTimeout("jQuery('#ajaxLoading').fadeIn();", 200);

				ajaxLoaderSequenceCounter = 0;
				ajaxLoaderSequenceTimeout = setInterval(function($) {
					var counter = '';
					if(document.ajaxq !== undefined && document.ajaxq.q['testmatch'] !== undefined) {
						counter = document.ajaxq.q['testmatch'].length;
					}
					jQuery(document).attr('title', (ajaxLoaderSequenceCounter % 3 == 0 ? '|.. ' : (ajaxLoaderSequenceCounter % 3 == 1 ? '||. ' : '||| ')) + ' ' + counter + ' ' + originalPageTitle); 
					ajaxLoaderSequenceCounter++;
					}, 500)
			});
		jQuery(document).ajaxStop(function($){
				clearTimeout(ajaxLoaderTimeout);
				clearInterval(ajaxLoaderSequenceTimeout);
				jQuery(document).attr('title', originalPageTitle);
				jQuery('#ajaxLoading').fadeOut();
			});

<?php 
/** inject onload data into graph here **/

?>
		
	var plot;

	// 0-pad a string to a certain length 
	function pad(num, length){
		var s = num+"";
		while (s.length < length) s = "0" + s;
		return s;
	}

	// time tick formatter for the graph
	function timeTickFormatter(val, axis) {
		var date = new Date(val);
		return pad(date.getDay(),2) + '.' + pad((date.getMonth()+1),2) + '.' + date.getFullYear() + '<br/>' + pad(date.getHours(),2) + ':' + pad(date.getMinutes(),2) + ':' + pad(date.getSeconds(),2) + ',' + pad(date.getMilliseconds(),3);
	}

	// configure graph
	$(function() {
		var lastResult = null;	
		var displayConfig = $("#displayConfig");

		var options = {
			canvas: false,
			lines: {
				show: true,
				lineWidth: 0.4
			},
			points: {
				show: true,
				radius: 0.5
			},
			xaxis: {
				tickDecimals: 0,
				//tickSize: 1000,
				minTickSize: 10,
				tickFormatter: timeTickFormatter,
				ticks: 9
			},
			yaxis: {
				zoomRange: false,
				panRange: false
			},
			zoom: {
				interactive: true
			},
			pan: {
				interactive: true
			},
			grid: {
				markings: [ { yaxis: { from: 0, to: 0 }, color: "#aaaaaa", lineWidth: 2 } ],
				backgroundColor: { colors: ["#fff", "#fff"] }
			},
			shadowSize: 0
		};
		var extendedOptions = $.extend(true, {}, options, {});

		var alreadyFetched = {};

		// clear graph
		$("a.clear").click(function () {
			data = [];
			displayedData = [];
			alreadyFetched = [];
			plot = $.plot('#graph', [], options);
			return false;
		});

		// apply selection in graph
		$("#graph").bind("plotselected", function (event, ranges) {

			// clamp the zooming to prevent eternal zoom
			if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {
				ranges.xaxis.to = ranges.xaxis.from + 0.00001;
			}

			if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {
				ranges.yaxis.to = ranges.yaxis.from + 0.00001;
			}

			// do the zooming
			extendedOptions = $.extend(true, {}, options, {
				xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
				yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
			}); 
			plot = $.plot("#graph", displayedData, extendedOptions);
		});

		// reset selection inside graph
		$("a.reset_selection").click(function () {
			resetSelection();
			plotData(displayedData);
			return false;
		});
		function resetSelection() {
			extendedOptions = $.extend(true, {}, options, {});			
		}

		// save image of current graph
		$("a.save_image").click(function () {
			saveImage();
		});
		function saveImage() {
			$("#graph").css('height', '100%');
			$("#graph").css('width', '100%');
			var axes = plot.getAxes();
			extendedOptions = $.extend(true, {}, options, {
				canvas: true,
				legend: {
					type: 'canvas'
					},
				xaxis: { min: axes.xaxis.min, max: axes.xaxis.max },
				yaxis: { min: axes.yaxis.min, max: axes.yaxis.max, position: "right",
					alignTicksWithAxis: 1 }
			});
			plotData(displayedData);
			
			$('#graphimg').attr('src', plot.getCanvas().toDataURL("image/png"));

			$("#graph").css('height', '100%');
			$("#graph").css('width', '100%');
			extendedOptions = $.extend(true, {}, extendedOptions, {
				canvas: false
			});
			plotData(displayedData);
		}

		// handle form of datasets
		$("#datasets_form").submit(function (event) {
			/* stop form from submitting normally */
			event.preventDefault();

			/* get some values from elements on the page: */
			var form = $( this );
			var formData = form.serialize();

			var dataurl = form.attr( 'action' );

			function onDataReceived(dataReceived) {
				data = dataReceived.series;
				plotData(data);
			}

			$.ajax({ url: dataurl, type: "POST", data: formData, dataType: "json", success: onDataReceived });
			return false;
		});

		// test matching between two datasets sliding a window over the pattern
		$("#button_test_matching").click(function (event) {
			event.preventDefault();
			var form = $( '#datasets_form' );
			var stepWidth = parseInt(form.find('input[name=step_width]').val());
			testMatch(stepWidth);
		});

		// test matching between all selected dataset pairs
		$("#button_test_matching_across_datasets").click(function (event) {
			event.preventDefault();
			var form = $( '#datasets_form' );
			var pairs = form[0].pair;
			var referenceDatasets = new Array();
			var patternDatasets = new Array();

			// generate unique pairs
			for(var i = 0; i < pairs.length; i++) {
				if(pairs[i].checked) {
					var pair = pairs[i].value.split('|');
					referenceDatasets.push(pair[0]);
					patternDatasets.push(pair[1]);
				}
			}
			referenceDatasets = $.unique(referenceDatasets);
			patternDatasets = $.unique(patternDatasets);

			var allPairings = new Array();
			for(var i = 0; i < referenceDatasets.length; i++) {
				for(var j = 0; j < patternDatasets.length; j++) {
					allPairings.push(new Array(referenceDatasets[i], patternDatasets[j]));
				} 
			}
			
			testMatching(allPairings);
		});

		// show matching results in new window
		$("#button_show_matching_new_window").click(function (event) {
			event.preventDefault();
			showMatchingResultsInNewWindow();
		});
		
		// show matching resutls
		$("#button_show_matching").click(function (event) {
			event.preventDefault();
			showMatchingResults();
		});

		var testMatchResults = new Array();
		var summary = new Object();
		var summaryTable = $('<table />');
		var resultTable = $('<table />');
		var pairingTable = $('<table />');
		var scatterPlot = $('<div />');
		var legendText = $('<div />'); 
		
		function prepareResultCollector() {
			summary = new Object();
			summary.matchingScores = {};
			summary.isPair = {};
			summary.matchingLag = {};
			summary.rfiles = [];
			summary.pfiles = [];
			summary.bestMatch = 0;
			
			$('#collector').html('');
			summaryTable = $('<table />');
			summaryTable.appendTo('#collector');

			scatterPlot = $('<div />');
			scatterPlot.attr('style', 'position: relative; width: 1000px; height: 52px; border: 1px solid black; background-image: linear-gradient(left, grey 0%, white 100%);');
			scatterPlot.appendTo('#collector');

			legendText = $('<div />');
			legendText.html('<p>Scatter plot: left means totally bad match (0.0), right means good metch (1.0), actual pairs are green dots, imitated or cross-paired pairs are red.</p>'
					+ '<p>Table: Matching scores are mapped to the text\'s color. Cells with a thick border are results for actual pairs.');
			legendText.appendTo('#collector');
			

			pairingTable = $('<table />');
			pairingTable.appendTo('#collector');
			
			resultTable = $('<table />');
			resultTable.appendTo('#collector');
		}

		var uid = (function(){var id=0;return function(){if(arguments[0]===0)id=0;return id++;}})();

		// print/store matching results
		function onResultReceived(dataReceived) {
			var table = resultTable;
			
			testMatchResults.push(dataReceived);
			data = dataReceived.series;

			var patternInfo = dataReceived.patternInfo.name + ',<br/> ' + dataReceived.patternInfo.comment + ',<br/> ' + dataReceived.patternFile;
			var referenceInfo = dataReceived.referenceInfo.name + ',<br/> ' + dataReceived.referenceInfo.comment + ',<br/> ' + dataReceived.referenceFile;

			if(summary.pfiles.indexOf(dataReceived.patternFile) < 0) {
				summary.pfiles.push(dataReceived.patternFile);
			}
			if(summary.rfiles.indexOf(dataReceived.referenceFile) < 0) {
				summary.rfiles.push(dataReceived.referenceFile);
			}
			var refIndex = summary.rfiles.indexOf(dataReceived.referenceFile);
			var patIndex = summary.pfiles.indexOf(dataReceived.patternFile);

			if(summary.matchingScores[refIndex] == undefined) {
				summary.matchingScores[refIndex] = {};
			}
			if(summary.isPair[refIndex] == undefined) {
				summary.isPair[refIndex] = {};
			}
			if(summary.matchingLag[refIndex] == undefined) {
				summary.matchingLag[refIndex] = {};
			}

			summary.isPair[refIndex][patIndex] = dataReceived.isPair;
			summary.matchingScores[refIndex][patIndex] = dataReceived.bestMatch;
			summary.matchingLag[refIndex][patIndex] = dataReceived.lag;
			
			if(summary.bestMatch < dataReceived.bestMatch) {
				summary.patternInfo = patternInfo;
				summary.referenceInfo = referenceInfo;
				summary.bestMatch = Math.max(summary.bestMatch, dataReceived.bestMatch);

				summaryTable.html('');
				summaryTable.html('<tr><td>Pattern Info</td><td>'+summary.patternInfo+'</td></tr>'+
						'<tr><td>Reference Info</td><td>'+summary.referenceInfo+'</td></tr>'+
						'<tr><td>Best Match</td><td>'+summary.bestMatch+'</td></tr>');
			}

			scatterPlot.html('');

			pthtml = '<tr><td></td>';
			for(var i = 0; i < summary.pfiles.length; i++) {
				pthtml += '<td>' + summary.pfiles[i] + '</td>';
			}
			pthtml += '</tr>';
			for(var i = 0; i < summary.rfiles.length; i++) {
				pthtml += '<tr><td>' + summary.rfiles[i] + '</td>';
				for(var j = 0; j < summary.pfiles.length; j++) {
					pthtml += '<td';
					if(summary.isPair[i][j]) {
						pthtml += ' style="border: 2px solid black;"';
					}
					pthtml += '>';
					if(summary.matchingScores[i] != undefined && summary.matchingScores[i][j] != undefined) {
						var resultId = summary.rfiles[i] + '_' + summary.pfiles[j];
						var anchor = $('<a />');
						anchor.attr('href', '#' + resultId);
						anchor.attr('title', summary.matchingScores[i][j]);
						anchor.appendTo(scatterPlot);
						
						var datapoint = $('<div />');
						datapoint.attr('style', 'position: absolute; background: ' + (summary.isPair[i][j] ? '#0a0' : '#f00') + '; top: ' + (summary.isPair[i][j] ? Math.round(Math.random() * 6)*3 : 20+Math.round(Math.random() * 10)*3) + 'px; left: ' + Math.round(summary.matchingScores[i][j] * 100) +'%; width: 5px; height: 5px;');
						datapoint.appendTo(anchor);

						pthtml += '<a href="#'+resultId+'"><span style="color:hsl(' + Math.round(summary.matchingScores[i][j] * 165 - 30) + ', 100%, 40%);">' + summary.matchingScores[i][j] + '</span></a>';
					}
					if(summary.matchingLag[i] != undefined && summary.matchingLag[i][j] != undefined) {
						pthtml += ' (' + summary.matchingLag[i][j] + 'ms)';
					}
					pthtml += '</td>';
					
				}	
				pthtml += '</tr>';
			}
			pairingTable.html(pthtml);
			
			var tr = $('<tr />');
			var td = $('<td />'); td.appendTo(tr);
			var anchor = $('<a />'); anchor.appendTo(td); anchor.attr('name', dataReceived.referenceFile + '_' + dataReceived.patternFile);
			var details = $('<table />'); details.appendTo(td);
			var dtr = $('<tr />').appendTo(details);
			$('<td />').html('Pattern').appendTo(dtr);
			$('<td />').html(patternInfo).appendTo(dtr);
			var dtr = $('<tr />').appendTo(details);
			$('<td />').html('Reference').appendTo(dtr);
			$('<td />').html(referenceInfo).appendTo(dtr);
			var dtr = $('<tr />').appendTo(details);
			$('<td />').html('Best Match').appendTo(dtr);
			$('<td />').html(dataReceived.bestMatch).appendTo(dtr);
			var dtr = $('<tr />').appendTo(details);
			$('<td />').html('Offset of match (ms)').appendTo(dtr);
			$('<td />').html(dataReceived.movedBy).appendTo(dtr);
//			var dtr = $('<tr />').appendTo(details);
//			$('<td />').html('Match Index').appendTo(dtr);
//			$('<td />').html(dataReceived.matchIndex).appendTo(dtr);
//			var dtr = $('<tr />').appendTo(details);
//			$('<td />').html('Pattern Start Trim').appendTo(dtr);
//			$('<td />').html(dataReceived.startOffset).appendTo(dtr);
			var dtr = $('<tr />').appendTo(details);
			$('<td />').html('Matcher').appendTo(dtr);
			var detailsMatcher = $('<table />');
			var obj = dataReceived.matcher;
			for (var p in obj) {
		        if (obj.hasOwnProperty(p)) {
		        	var dmtr = $('<tr />').appendTo(detailsMatcher);
		        	$('<td />').html(p).appendTo(dmtr);
		        	$('<td />').html(obj[p]).appendTo(dmtr).css('text-align', 'right');
		        }
		    }
			$('<td />').html(detailsMatcher).append('\n').appendTo(dtr);
			
			var td = $('<td />'); td.appendTo(tr);
			var types = [];
			displayConfig.find("input:checked").each(function () {
					if($(this).attr("name") != 'matching') {
						types.push($(this).attr("name"));
					}
				});
			resetSelection();
			plotData(data, types);
			saveImage();
			var img = $('<img />');
			img.attr('src', $('#graphimg').attr('src'));
			img.attr('width', 600); 
			img.appendTo(td);

			var td = $('<td />'); td.appendTo(tr);
			resetSelection();
			extendedOptions = $.extend(true, {}, options, {
				yaxis: { min: 0.0, max: 1.0 }
			});
			var types = [];
			types.push('matching');
			plotData(data, types);
			saveImage();
			var img = $('<img />');
			img.attr('src', $('#graphimg').attr('src'));
			img.attr('width', 600);
			img.appendTo(td);

			tr.appendTo(table); 
			//$('.graph-container').clone().appendTo('#collector');
		}

		// test matching by moving a window over the pattern
		function testMatch(stepWidth) {
			var form = $( '#datasets_form' );

			prepareResultCollector();
			
			var tPatternOffset = form.find('input[name=pattern_offset]').val();
			for(var startTrim = 0; startTrim < 30000; startTrim += stepWidth) {
				form.find('input[name=pattern_offset]').val(startTrim);

				var formData = form.serialize();
				var dataurl = form.attr( 'action' );
				$.ajaxq('testmatch', { url: dataurl, type: "POST", data: formData, dataType: "json", success: onResultReceived });
			}
			form.find('input[name=pattern_offset]').val(tPatternOffset);
		}

		// test matching between dataset pairs
		function testMatching(datasetPairs) {
			var form = $( '#datasets_form' );

			var property = form[0]['used_property'].value; 

			prepareResultCollector();
			
			for(var i = 0; i < datasetPairs.length; i++) {
				var pair = datasetPairs[i];
				console.log(pair);
				form[0][pair[0]+'['+property+']'].checked = true;
				form[0][pair[1]+'['+property+']'].checked = true;
				
				var formData = form.serialize();
				
				var dataurl = form.attr( 'action' );
				$.ajaxq('testmatch', { url: dataurl, type: "POST", data: formData, dataType: "json", success: onResultReceived });

				form[0][pair[0]+'['+property+']'].checked = false;
				form[0][pair[1]+'['+property+']'].checked = false;
			}
			
		}

		// open new window with matching results
		function showMatchingResultsInNewWindow() {
			newwindow=window.open();
			newwindow.location.href = window.location.href;
			newdocument=newwindow.document;

			html = '<html><head><style>';
			
			var list = null;
			with (document.styleSheets[0]) {
			    if (typeof cssRules != "undefined")
			        list = cssRules;
			    else if (typeof rules != "undefined")
			        list = rules;
			}
			css = list;
			cssString = '';
			for(var i = 0; i < css.length; i++) {
				cssString += css[i].cssText;
			} 
			html += cssString;
			html += '</style><script type="text/Javascript"><\/script>\n</head><body>';
			html += $('#collector').html();
			html += '</body></html>';

			newdocument.write(html);
			newdocument.close();
		}

		function showMatchingResults() {
			$('a[href="#tabMatching"]').click();
		}

		// plot graph
		function plotData(data, displayedTypes) {
			if(typeof displayedTypes === 'undefined') {
				var displayedTypes = [];
				displayConfig.find("input:checked").each(function () {
					displayedTypes.push($(this).attr("name"));
					});
			}

			displayedData = [];
			$.each(data, function(index, value) {
				if (value.type && 0 <= $.inArray(value.type, displayedTypes)) {
					displayedData.push(value);
				}
			});
			plot = $.plot("#graph", displayedData, extendedOptions);
		}
		
		displayConfig.find("input").click(function() {plotData(data);});

		// plot data if injected by php
		if(data.length > 0) {
			plotData(data);
		}


		// Tabs
		$('ul.tabs').each(function(){
			var $active, $content, $links = $(this).find('a');
		    $active = $links.first().addClass('active');
		    $content = $($active.attr('href'));
		    $links.not(':first').each(function () { $($(this).attr('href')).hide(); });
		    $(this).on('click', 'a', function(e){
		        $active.removeClass('active'); $content.hide(); $active = $(this); $content = $($(this).attr('href')); $active.addClass('active'); $content.show();
				e.preventDefault();
		    });
		});
	});

	</script>
		
</head>
<body>
<ul class='tabs'>
    <li><a href='#tab1'>Übersicht</a></li>
    <li><a href='#tabMatching'>Matching Ergebnisse</a></li>
</ul>
<div id="tab1">
	<div class="container">
	<img src="" id="graphimg" width="80" style="float: right; opacity: 0.2;"/>
		<h1>Analysis</h1>
		<p>by Sebastian H&ouml;ninger</p>
		<!-- Graph Container -->
		<div class="graph-container">
				<div id="graph" class="graph-placeholder"></div>
				<a class="clear" href="#" >clear</a> |
				<a class="reset_selection" href="#" >reset selection</a> |
				<a class="save_image" href="#" >save image</a>
		</div>
		
		<!-- configuration of displayed time series in graph -->
		<div id="displayConfig">
		<?php foreach($valueTypes as $type) {
			echo '<input id="id'.$type.'" type="checkbox" checked="checked" name="'.$type.'" /><label class="checkbox" for="id'.$type.'">'.$type.'</label>&nbsp;&nbsp;';
		}?>
		</div>
		<div id="files">
		<form id="datasets_form" action="?action=analysis_service" method="POST">
		<input type="submit" name="compute" value="Compute & Load" />
		<input type="button" id="button_show_matching" name="show_matching" value="Show Results" />
		<input type="button" id="button_show_matching_new_window" name="show_matching_new_window" value="Show Results in New Window" />
		<!-- setting for plotting and matching -->
		<table><tr><td>
			<input type="checkbox" name="compute_acceleration" checked="checked" /> recompute acceleration from position<br/>
			<input type="checkbox" name="move_to_zero" /> move to zero<br/>
			<input type="checkbox" name="smooth" checked="checked" /> show smoothed raw data<br/>
			<input type="checkbox" name="compute_correlation" checked="checked" /> compute correlation<br/>
			<input type="checkbox" name="show_resampled_data" checked="checked" /> show resampled data<br/>
			Start Trim Step Width <input type="text" name="step_width" value="5000" /><br/>
			Metric <select name="used_metric">
				<option value="imse">Inverse Mean Squared Error</option>
				<option value="inrmse">Inverse Root Mean Squared Error</option>
				<option value="imsea">Inverse Mean Squared Absolute Error</option>
				<option value="imseMirror">Inverse Mean Square Error (max'ed with imse of mirrored pattern)</option>
				<option value="pearsonSquared" selected>Pearson Correlation Squared</option>
				<option value="zncc">Zero Normalized Cross Correlation</option>
				</select><br/>
			Property <select name="used_property">
				<option value="x">x</option>
				<option value="y">y</option>
				<option value="z">z</option>
				<option value="pc_proj_norm">Length of Projection on Principal Component</option>
				<option value="pc_proj_value" selected>Value of Projection along Principal Component</option>
				<option value="acc_norm">Length of Acceleration</option>
				</select><br/>
			<input type="button" id="button_test_matching" name="test_matching" value="Test moving matching" />
			<input type="button" id="button_test_matching_across_datasets" name="test_matching_across_datasets" value="Test all datasets" />
		</td>
		<td style="padding-left: 1em;">
			
		</td>
		<td style="padding-left: 1em;">
			Resolution <input type="text" name="resolution" value="33" /><br/>
			Max Signal Gap <input type="text" name="maximum_signal_gap" value="150" /><br/>
			Search Window <input type="text" name="search_window" value="3000" /><br/>
			Minimum Overlap <input type="text" name="minimum_overlap" value="8000" /><br/>
			Max Pattern Length <input type="text" name="pattern_length" value="10000" /><br/>
			Pattern Start Trim <input type="text" name="pattern_offset" value="<?=(isset($config['authentication']['patternTrimStart']) ? $config['authentication']['patternTrimStart'] : 8000)?>" /><br/>
			Reference Start Trim <input type="text" name="reference_offset" value="0" /><br/>
			Smoothing tau (ms) <input type="text" name="smoothing_tau" value="<?=(isset($config['authentication']['smoothingTau']) ? $config['authentication']['smoothingTau'] : 100)?>" /><br/>
		</td></tr></table>
		
		<!-- list of datasets -->
		<div style="height: 300px; overflow: auto;">
		<table style="font-size: 0.75em"><thead><tr><th>Active<input type="checkbox" name="pairToggle" onclick="$(':checkbox[name=pair]').prop('checked', this.checked);" /></th><th>File</th><th>Name</th><th>Time</th>
		<?php foreach($valueTypes as $type) { 
			if($type == 'matching') continue;
			echo '<th>'.$type.' </th>'; 
		}?></thead>
	<?php 
	$files = array();
	$files = Dataset::listDatasets('', true);
	$lastPairId = null;
	$colors = array('kinect' => '#0a0', 'browser' => '#a00', 'visual' => '#00a');
	$pairs = array();
	
	// load pairs (tracked, imitated
	$loadedData[] = array();
	foreach($files as $file) {
		$fileid = $file['path'];
		$d = json_decode(file_get_contents($dataDirectory.$file['path']));
		if(isset($d->acc)) unset($d->acc);
		if(isset($d->accG)) unset($d->accG);
		if(isset($d->pos)) unset($d->pos);
		if(isset($d->rot)) unset($d->rot);
		$loadedData[$fileid] = $d;
	}
	foreach($loadedData as $fileid => $data) {
		$pairId = isset($data->pairId) ? $data->pairId : null;
		if($pairId != null) {
			if(!isset($pairs[$pairId]) || !is_array($pairs[$pairId])) {
				$pairs[$pairId] = array();
			}
			$pairs[$pairId][strpos($fileid, 'visual') !== false ? 0 : 1] = $fileid;
		}
	}
	foreach($loadedData as $fileid => $data) {
		$imitatedPairId = isset($data->imitatedPairId) ? $data->imitatedPairId : null;
		if($imitatedPairId != null) {
			if(isset($pairs[$imitatedPairId])) {
				$pairs['imi_'.$imitatedPairId] = array($pairs[$imitatedPairId][0], $fileid);
			}
		}
	}
	
	// show the datasets, one row per dataset
	foreach($files as $file) {
		$fileid = $file['path'];
		$data = json_decode(file_get_contents($dataDirectory.$file['path']));
		$pairId = isset($data->pairId) ? $data->pairId : null;
		$imitatedPairId = isset($data->imitatedPairId) ? $data->imitatedPairId : null;
		echo '<tr class="';
		if($pairId == null || $lastPairId != $pairId) {
			echo 'new_section';
		}
		echo '">';
		echo '<td>';
		if($imitatedPairId != null) {
			if(count($pairs['imi_'.$imitatedPairId])) {
				echo '<input type="checkbox" name="pair" value="';
				echo $pairs['imi_'.$imitatedPairId][0].'|'.$pairs['imi_'.$imitatedPairId][1];
				echo '" />';
			}
		} else if($pairId != null && $lastPairId != $pairId) {
			if(count($pairs[$pairId])) {
				echo '<input type="checkbox" name="pair" value="';
				echo $pairs[$pairId][0].'|'.$pairs[$pairId][1];
				echo '" />';
			}
		}
		echo '</td>';
		echo '<td>'.$file['type'].' <a class="dataset" style="color: '.(isset($colors[$file['type']]) ? $colors[$file['type']] : '#000').';" href="'.$dataDirectory.$file['path'].'">'.$file['filename'].'</a></td>';
		echo '<td>'.$data->name.'</td>';
		
		$timestamp = $data->timestamp;
		if(isset($data->estimatedServerClockLag)) {
			$timestamp -= $data->estimatedServerClockLag;
		}
		echo '<td>'.date('H:i:s', $timestamp/1000).'.'.str_pad(abs($timestamp % 1000), 3, "0", STR_PAD_LEFT).'</td>';
		
		// checkboxes for time series contained in the dataset
		foreach($valueTypes as $type) {
			if($type == 'matching') continue;
			$available = true;
			echo '<td>';
			if(array_key_exists($type, $valueDataSource)) {
				if(is_array($valueDataSource[$type])) {
					$t = false;
					foreach($valueDataSource[$type] as $property) {
						if(isset($data->{$property})) {
							$t = true;
						}
					}	
					$available = $t;
				} else {
					if(!isset($data->{$valueDataSource[$type]})) {
						$available = false;
					}
				}
			}
			
			if($available) echo '<input type="checkbox" name="'.$fileid.'['.$type.']" />';
			echo '</td>';
		}
		echo '</tr>';	
		$lastPairId = $pairId;
	}
	?>		
		</table></div>	
		</form></div>
	</div>
</div>

<!-- container for matching results -->
<div id="tabMatching">
	<div id="collector">
	</div>
</div>
</body>
</html>