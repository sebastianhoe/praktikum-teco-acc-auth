<?php
// store data from kinect
// TODO defined secure source ip!
if ( !defined('CP') ) { die("Invalid access.");}

$dataPath = $dataDirectory.'visual/';

$sample = null;
if(isset($_POST['sample'])) {
	$sample = $_POST['sample'];
	var_dump($sample);
} else {
	$requestBody = file_get_contents('php://input');
	$sample = json_decode($requestBody);
}

if(!is_null($sample)) {
	$fp = fopen($dataPath.date('Ymd_His').'_'.round(substr(microtime(),0,6)*1000).'.json', 'w');
	fwrite($fp, json_encode($sample));
	fclose($fp);
}
?>