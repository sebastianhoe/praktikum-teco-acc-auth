<?php
class Matcher {
	public $offsetLimit = 60; // maximum timer offset in seconds
	public $resolution = 33; // milliseconds sampling interval
	public $maximumGapLength =  150; // gap in data-samples in milliseconds
	public $searchWindow = 3000; // milliseconds
	public $searchOnlyInWindow = true;
	public $minimumOverlap = 8000; // minimal milliseconds of overlapping samples (considering gaps, lengths,...)
	public $usedPatternLength = 10000;
	public $usedPatternOffset = 8000;
	public $usedReferenceOffset = 0;
	public $usedMetric = 'pearsonSquared';
	public $smoothingTau = 100;
	public $referenceProperty = 'pc_proj_value';
	public $patternProperty = 'pc_proj_value';
	
	/**
	 * Search for matching datasets in refrence directory
	 * @param Dataset $pattern Pattern to match.
	 * @param string $referencDirectory Directory to search for matching datasets based on timestamp.
	 */
	public function searchMatch(Dataset $pattern, $referencDirectory) {
		$references = Dataset::listDatasets($referencDirectory, true);
		
		$result = array();
		//$patternTimestamp = $pattern->getTimestamp();
		$patternTimestamp = Dataset::timestampFromFilename($pattern->getFilename());
		
		foreach($references as $key => $referenceFile) {
			//$referenceTimestamp = $reference->getTimestamp();
			$referenceTimestamp = Dataset::timestampFromFilename($referenceFile['filename']);
			
			if(abs($referenceTimestamp - $patternTimestamp) <= $this->offsetLimit) {
				$reference = new Dataset();
				$reference->load($referenceFile['path']);
				$result[$key] = $this->match($pattern, $reference);
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * Compute match between two datasets.
	 * @param Dataset $pattern
	 * @param Dataset $reference
	 * @param array $options
	 */
	public function match(Dataset $pattern, Dataset $reference, $options = array()) {
		if(!isset($options['referenceProperty'])) {
			$options['referenceProperty'] = $this->referenceProperty;
		}
		$referenceProperty = $options['referenceProperty'];
		if(!isset($options['patternProperty'])) {
			$options['patternProperty'] = $this->patternProperty;
		}
		$patternProperty = $options['patternProperty'];
		
		$supportedProperties = array('x','y','z','acc_norm', 'pc_proj_norm', 'pc_proj_value');
		if(!in_array($referenceProperty, $supportedProperties) || !in_array($patternProperty, $supportedProperties)) {
			$result = array(
					'error' => 'unsupported property type for matching'
					);
			return $result;
		}
		
		$result = null;
		$minOverlaps = round($this->minimumOverlap / $this->resolution);
		
		// prepare pattern values / time series
		if($pattern->getPosition() != NULL) {
			$pattern->computeAcceleration();
		}
		$property = $pattern->getAcceleration();
		if(in_array($patternProperty, array('pc_proj_norm', 'pc_proj_value'))) {
			$property = Dataset::principalComponentProjection($pattern->getAcceleration());
			$propertyPC = Dataset::principalComponent($pattern->getAcceleration(), true);
		}
		$values = array();
		foreach($property as $pKey => $p) {
			//$values[] = array(isset($p->ts) ? $p->ts - $pattern->getEstimatedServerOffset() : $pKey, $p->{$patternProperty});
			if($patternProperty == 'acc_norm' || $patternProperty == 'pc_proj_norm') {
				$value = sqrt($p->x*$p->x + $p->y*$p->y + $p->z*$p->z)	;
				//$value = (100+$p->x) + (100+$p->y) + (100+$p->z) - 300;
			} else if($patternProperty == 'pc_proj_value') {
				$value = SPL::project3D(array($p->x, $p->y, $p->z), $propertyPC);
			} else {
				$value = $p->{$patternProperty};
			}
			$values[] = array(isset($p->ts) ? $p->ts - $pattern->getEstimatedServerClockLag() : $pKey, $value);
		}
		$patternTimeSeries = $values;
		
		// prepare reference values / time series
		if($reference->getPosition() != NULL) {
			$reference->computeAcceleration();
		}
		$property = $reference->getAcceleration();
		// TODO compute pcp only from trimmed pattern and trimmed reference!
		if(in_array($referenceProperty, array('pc_proj_norm', 'pc_proj_value'))) {
			$property = Dataset::principalComponentProjection($reference->getAcceleration());
			$propertyPC = Dataset::principalComponent($reference->getAcceleration(), true);
		}
		$values = array();
		foreach($property as $pKey => $p) {
			//$values[] = array(isset($p->ts) ? $p->ts + $reference->getEstimatedServerOffset() : $pKey, $p->{$referenceProperty});
			if($referenceProperty == 'acc_norm' || $referenceProperty == 'pc_proj_norm') {
				$value = sqrt($p->x*$p->x + $p->y*$p->y + $p->z*$p->z);
				//$value = (100+$p->x) + (100+$p->y) + (100+$p->z) - 300;
			} else if($referenceProperty == 'pc_proj_value') {
				$value = SPL::project3D(array($p->x, $p->y, $p->z), $propertyPC);
			} else {
				$value = $p->{$referenceProperty};
			}
			$values[] = array(isset($p->ts) ? $p->ts - $reference->getEstimatedServerClockLag() : $pKey, $value);
		}
		$referenceTimeSeries = $values;
		
		
		// compute result
		$result = $this->matchTimeSeries($patternTimeSeries, $referenceTimeSeries, $options);
		return $result;
	}
	
	/**
	 * 
	 * Match two time series (timestamp as key of array elements)
	 * @param array $pattern
	 * @param array $reference
	 * @param array $options
	 * 		bool resample (default true)
	 */
	public function matchTimeSeries($pattern, $reference, $options = array()) {
		$result = null;
		$minimumOverlaps = round($this->minimumOverlap / $this->resolution);
		
		// php copies arrays by default, no reference type
		$originalPattern = $pattern; 
		$originalReference = $reference;
		
		// resample if not turned off
		if(!isset($options['resample']) || $options['resample'] === true) {
			$reference = SPL::resample($originalReference, $this->resolution, $this->maximumGapLength);
			$pattern = SPL::resample($originalPattern, $this->resolution, $this->maximumGapLength);
		}
 		
		if($this->smoothingTau > 0) {
			Dataset::computeIEMA($reference,$this->smoothingTau);
 			Dataset::computeIEMA($pattern,$this->smoothingTau);
		}
		
		// offset from pattern start
		if($this->usedPatternOffset > 0) {
			array_splice($pattern, 0, ceil($this->usedPatternOffset/$this->resolution));
		}
		
		// trim length of pattern
		if($this->usedPatternLength < count($pattern) * $this->resolution) {
			array_splice($pattern, ceil($this->usedPatternLength/$this->resolution));
		}
		
		if($this->usedReferenceOffset > 0) {
			array_splice($reference, 0, ceil($this->usedReferenceOffset/$this->resolution));
		}
		
		$referenceLength = count($reference);
		$patternLength = count($pattern);
		//$pattern = SPL::normalize($pattern);
		$patternAverage = SPL::average($pattern);
		$patternVariance = SPL::variance($pattern);
		
		// scale from m/s2 to g
		//$pattern = SPL::scale($pattern, 1/9.81);
		//$reference = SPL::scale($reference, 1/9.81);
		
		
		$crosscorrelation = array();
		$referenceIndex = 0;
		
		// zero-padding of reference and full swipe crosscorrelation
		// TODO search only in window if set
		$paddedReference = array();
		for($i = -$patternLength; $i < 2*$patternLength + $referenceLength; $i += 1) {
			if($i < 0) {
				$paddedReference[] = array($reference[0][0] + $i*$this->resolution, null);
			} else if($i < $referenceLength) {
				$paddedReference[] = $reference[$i];
			} else {
				$paddedReference[] = array($reference[0][0] + $i*$this->resolution, null);
			}
		}
		
		// fold time series
		$startingOffset = + ($pattern[0][0] - $reference[0][0]);
		for($i = $minimumOverlaps - 1; $i < $patternLength + $referenceLength - $minimumOverlaps + 1; $i++) {
			// use string-index, because numerical index is truncated if too big
			$zncc = call_user_func(array('SPL', $this->usedMetric), $pattern, $paddedReference, $i, array(
						'patternAverage' => $patternAverage,
						'patternVariance' => $patternVariance,
						'patternIsNormalized' => false,
						'detailedResult' => true,
						), true);
			if($zncc['overlaps'] > $minimumOverlaps) {
				$crosscorrelation[((-$patternLength + $i)*$this->resolution)."" - $startingOffset] = abs($zncc['value']);
			} else {
				$crosscorrelation[((-$patternLength + $i)*$this->resolution)."" - $startingOffset] = null;
			}
		}
		
		// collect results
		$matchIndex = SPL::realMaxIndex($crosscorrelation);
		$bestMatch = $crosscorrelation[$matchIndex];
		$matchingScores = $crosscorrelation;
		$result = array(
			'pattern' => $pattern,
			'reference' => $reference,
			'matchingScores' => $matchingScores,
			'matchingIndex' => $matchIndex,
			'bestMatch' => $bestMatch,
			'startingOffset' => $startingOffset,
		);
		return $result;
	}
}