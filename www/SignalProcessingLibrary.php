<?php 
/*
 * Signal Processing Library
*
* Library that provides function for averages, variances, resampling etc..
* Function consider null-Values as gaps and compute average etc. accordingly.
*
* author: Sebastian Höninger
*/
class SPL { // alias SignalProcessingLibrary is available

	/*
	 * Counts number of elements != null
	*/
	public static function count($in) {
		$c = 0;
		foreach($in as $val) {
			if($val[1] !== null) $c++;
		}
		return $c;
	}

	/**
	 * 
	 * Compute exponential moving average
	 * @param float[] $in
	 * @param float $alpha Smoothing paramter
	 */
	public static function ema($in, $alpha = 0.1) {
		$out = array();
		$out[0] = @$in[0];
		for($i=1, $count = sizeof($in); $i<$count; $i++) {
			$out[$i] = $out[$i-1] + $alpha * ($in[$i] - $out[$i-1]);
		}
		return $out;
	}

	/**
	 * 
	 * Compute irregular time series exponential moving average
	 * @param float[] $timestamps Timestamp for entries of paramter $in
	 * @param float[] $in
	 * @param float $tau Smoothing paramter (correlates to smoothing window)
	 */
	public static function iema($timestamps, $in, $tau = 100.0) {
		$out = array();
		$out[0] = @$in[0];
		for($i=1, $count = sizeof($in); $i<$count; $i++) {
			$delta = abs($timestamps[$i] - $timestamps[$i-1]) / $tau;
			if($delta == 0) {
				$out[$i] = null;
				continue;
			}
			$w = exp(-$delta);
			$w2 = (1-$w) / $delta;
			$out[$i] = $out[$i-1] * $w + (1-$w2) * $in[$i] + ($w2 - $w) * $in[$i-1];
		}
		return $out;
	}

	/**
	 * 
	 * Compute centered exponential moving average (forward + backward ema to estimate non-lagging ema)
	 * @param float[] $in
	 * @param float $alpha Smoothing parameter
	 */
	public static function cema($in, $alpha = 0.1) {
		$forward = self::ema($in, $alpha);
		$backward = array_reverse(self::ema(array_reverse($in), $alpha));
		for($i=0, $count = sizeof($forward); $i<$count; $i++) {
			$out[$i] = ($forward[$i] + $backward[$i])/2.0;
		}
		return $out;
	}

	/**
	 * 
	 * Compute irregular time series centered exponential moving average
	 * @param float[] $timestamps Timestamp for entries of paramter $in
	 * @param float[] $in
	 * @param float $tau Smoothing paramter (correlates to smoothing window)
	 */
	public static function icema($timestamps, $in, $tau = 100.0) {
		$out = array();
		$forward = self::iema($timestamps, $in, $tau);
		$backward = array_reverse(self::iema(array_reverse($timestamps), array_reverse($in), $tau));
		for($i=0, $count = sizeof($forward); $i<$count; $i++) {
			$out[$i] = ($forward[$i] + $backward[$i])/2.0;
		}
		return $out;
	}

	/**
	 * 
	 * Compute average of time series ignoring null-values.
	 * @param float[][timestamp, value] $in
	 * @param int $from
	 * @param int $to
	 */
	public static function average($in, $from = 0, $to = null) {
		if($to == null) $to = count($in)-1;
		$sum = 0;
		$c = 0;
		for($i = $from; $i <= $to; $i++) {
			if(!isset($in[$i])) continue;
			if($in[$i][1] !== null) {
				$sum += $in[$i][1];
				$c++;
			}
		}
		return $sum/$c;
	}

	/**
	 * 
	 * Compute variance of time series ignoring null-values.
	 * @param float[][timestamp, value] $in
	 * @param int $from
	 * @param int $to
	 * @param float $average Average of time series to improve performance
	 */
	public static function variance($in, $from = 0, $to = null, $average = null) {
		if($to == null) $to = count($in)-1;
		if($average == null) $average = self::average($in, $from, $to);
		$sum = 0;
		$c = 0;
		for($i = $from; $i <= $to; $i++) {
			if(!isset($in[$i])) continue;
			if($in[$i][1] !== null) {
				$sum += ($in[$i][1] - $average) * ($in[$i][1] - $average);
				$c++;
			}
		}
		return $sum/$c;
	}
	
	/**
	 * 
	 * Normalize acceleration time series ignoring null values.
	 * @param float[][timestamp, value] $in
	 * @param int $from
	 * @param int $to
	 */
	public static function normalize($in, $from = 0, $to = null) {
		if($to == null) $to = count($in)-1;
		$average = self::average($in, $from, $to);
		$variance = self::variance($in, $from, $to, $average);
		$standardDeviation = sqrt($variance);
		$sum = 0;
		$out = array();
		$last = null;
		$holdCount = 0;
		for($i = $from; $i <= $to; $i++) {
			if(!isset($in[$i])) continue;
			$out[$i] = $in[$i];
			
			if($in[$i][1] !== null) {				
				$out[$i][1] = ($out[$i][1] - $average) / $standardDeviation;
				//$out[$i][1] = $out[$i][1] / $standardDeviation;
			}	
		}
		return $out;
	}
	
	/**
	 * 
	 * Scale acceleration time series ignoring null values.
	 * @param float[][timestamp, value] $in
	 * @param float $factor
	 */
	public static function scale($in, $factor = 1) {
		$out = array();
		foreach($in as $key => $value) {
			$out[$key] = $value;
			if($value[1] !== null) {
				$out[$key][1] = $value[1] * $factor;
			}
		}
		return $out;
	}
	
	/**
	 * 
	 * Compute inverse normalized root mean square error between time series.
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function inrmse($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);
		
		$patternAverage = isset($options['patternAverage']) ? $options['patternAverage'] : self::average($pattern);
 		$patternVariance = isset($options['patternVariance']) ? $options['patternVariance'] : self::variance($pattern);
 		$referenceAverage = self::average($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
 		$referenceVariance = self::variance($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
	
 		$variance = $patternVariance * $referenceVariance;
 		$standardDeviationPattern = sqrt($patternVariance);
 		$standardDeviationReference = sqrt($referenceVariance);
 		$standardDeviation = sqrt($patternVariance) * sqrt($referenceVariance);
	
 		if($variance == 0) {
 			return 0;
 		}
		
 		$sum = 0;
 		$min = 10000000;
 		$max = -10000000;
		$valueCount = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$valueCount++;
				if($pattern[$i][1] > $max) $max = $pattern[$i][1];
				if($pattern[$i][1] < $min) $min = $pattern[$i][1];
				$d = $pattern[$i][1] - $reference[$referenceStartIndex + $i][1];
				$sum +=  $d*$d;
			}
		}
	
		$result =  $sum / $valueCount;
		$result = sqrt($result) / ($max - $min);
		$result = 1/(1+$result);
	
		if($detailedResult) {
			return array(
					'value' => $result,
					'overlaps' => $valueCount
					);
		} else {
			return $result;
		}
	}
	
	/**
	 * 
	 * Compute inverse normalized mean square error between time series.
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function imse($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);	
		$patternAverage = isset($options['patternAverage']) ? $options['patternAverage'] : self::average($pattern);
 		$patternVariance = isset($options['patternVariance']) ? $options['patternVariance'] : self::variance($pattern);
 		$referenceAverage = self::average($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
 		$referenceVariance = self::variance($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
	
 		$variance = $patternVariance * $referenceVariance;
 		$standardDeviationPattern = sqrt($patternVariance);
 		$standardDeviationReference = sqrt($referenceVariance);
 		$standardDeviation = sqrt($patternVariance) * sqrt($referenceVariance);
	
 		if($variance == 0) {
 			return 0;
 		}
		
 		$sum = 0;
 		$min = 10000000;
 		$max = -10000000;
		$valueCount = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$valueCount++;
				if($pattern[$i][1] > $max) $max = $pattern[$i][1];
				if($pattern[$i][1] < $min) $min = $pattern[$i][1];
				$d = $pattern[$i][1] - $reference[$referenceStartIndex + $i][1];
				$sum +=  $d*$d;
			}
		}
	
		$result =  $sum / $valueCount;
		$result = sqrt($result) / ($max - $min);
		$result = $result * $result;
		$result = 1/(1+$result);
	
		if($detailedResult) {
			return array(
					'value' => $result,
					'overlaps' => $valueCount
					);
		} else {
			return $result;
		}
	}
	
	/**
	 * 
	 * Max of inverse mean squared error of pattern and inverted pattern
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function imseMirror($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);
		
		$result1 = self::imse($pattern, $reference, $referenceStartIndex, $options, $detailedResult);
		
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null) {
				$pattern[$i][1] = -$pattern[$i][1];
			}
		}
		
		$result2 = self::imse($pattern, $reference, $referenceStartIndex, $options, $detailedResult);
	
		
		if($detailedResult) {
			return array(
					'value' => max($result1['value'], $result2['value']),
					'overlaps' => $result1['overlaps'],
					);
		} else {
			return max($result1, $result2);
		}
	}
	
	/**
	 * 
	 * Inverese mean squared error of absolute values
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function imsea($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);		
		//$pattern = SPL::normalize($pattern);
		//$reference = SPL::normalize($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
 		$patternVariance = isset($options['patternVariance']) ? $options['patternVariance'] : self::variance($pattern);
 		$referenceVariance = self::variance($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
 		$variance = $patternVariance * $referenceVariance;
 		if($variance == 0) {
 			return 0;
	 	}
	
		$sum = 0;
		$valueCount = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$valueCount++;
				$d = (abs($pattern[$i][1]) - abs($reference[$referenceStartIndex + $i][1]));
				$sum +=  $d*$d;// / ($variance);
			}
		}
		
		$result =  $sum / $valueCount;
		$result = 1/(1+$result);
	
		if($detailedResult) {
			return array(
					'value' => $result,
					'overlaps' => $valueCount,
			);
		} else {
			return $result;
		}
	}
	
	/**
	 * 
	 * Pearson squared correlation between time series
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function pearsonSquared($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);
		$valueCount = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$valueCount++;
			}
		}
		
		$patternAverage = isset($options['patternAverage']) ? $options['patternAverage'] : self::average($pattern);
		$patternVariance = isset($options['patternVariance']) ? $options['patternVariance'] : self::variance($pattern);
		$referenceAverage = self::average($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
		$referenceVariance = self::variance($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);

		if($referenceVariance*$patternVariance == 0) {
			return 0;
		}
		$sum = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$t = ($pattern[$i][1] - $patternAverage) * ($reference[$referenceStartIndex + $i][1] - $referenceAverage);
				$sum +=  $t;
			}
		}
		$r = $sum / (sqrt($patternVariance * $valueCount) * sqrt($referenceVariance * $valueCount));
		$result = $r*$r;

		if($detailedResult) {
			return array(
					'value' => $result,
					'overlaps' => $valueCount
			);
		} else {
			return $result;
		}
		
	}

	/**
	 * 
	 * Zero Mean Normalized Cross Correlation between time series
	 * @param unknown_type $pattern
	 * @param unknown_type $reference
	 * @param unknown_type $referenceStartIndex
	 * @param unknown_type $options
	 * @param unknown_type $detailedResult
	 */
	public static function zncc($pattern, $reference, $referenceStartIndex, $options, $detailedResult = false) {
		$patternLength = count($pattern);
		$patternAverage = isset($options['patternAverage']) ? $options['patternAverage'] : self::average($pattern);
		$patternVariance = isset($options['patternVariance']) ? $options['patternVariance'] : self::variance($pattern);
		//echo 'compute reference statistics for '.$referenceStartIndex.'| patl: '.$patternLength.', refl: '.count($reference);
		$referenceAverage = self::average($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);
		$referenceVariance = self::variance($reference, $referenceStartIndex, $referenceStartIndex + $patternLength - 1);

		$variance = $patternVariance * $referenceVariance;
		$standardDeviation = sqrt($patternVariance) * sqrt($referenceVariance);

		if($variance == 0) {
			return 0;
		}
		$sum = 0;
		$valueCount = 0;
		for($i = 0; $i < $patternLength; $i++) {
			if($pattern[$i][1] !== null && $reference[$referenceStartIndex + $i][1] !== null) {
				$valueCount++;
				$sum += (($pattern[$i][1] - $patternAverage) * ($reference[$referenceStartIndex + $i][1] - $referenceAverage)) / $standardDeviation;
			}
		}

		$result =  $sum / $valueCount;

		if($detailedResult) {
			return array(
					'value' => $result,
					'overlaps' => $valueCount,
					'variance' => $variance);
		} else {
			return $result;
		}
	}

	/**
	 * 
	 * Resample time series to a certain resolution
	 * @param float[][timestamp, value] $in
	 * @param int $resolution Resolution in ms
	 * @param int $maxGap Maximum gap length in ms before values are nulled
	 */
	public static function resample($in, $resolution, $maxGap = 0x100000000) {
		$currentIndex = 0;
		$inCount = count($in);
		$out = array();
		for($ts = $in[0][0], $maxTs = $in[count($in)-1][0]; $ts <= $maxTs; $ts += $resolution) {
			$tsCurrent = $in[$currentIndex][0];
				
			// advance in input data while timestamp is smaller than next resampling timestamp
			while($currentIndex < $inCount && $tsCurrent < $ts) {
				$currentIndex++;
				$tsCurrent = $in[$currentIndex][0];
			}
				
			// keep previous timestamp in mind
			if($currentIndex > 0) {
				$tsPrevious = $in[$currentIndex-1][0];
			}
				
			// check gap and take value if exactly matched, otherwise linear interpolation
			if($currentIndex > 0 && ($tsCurrent - $tsPrevious) > $maxGap) {
				$value = null;
			} else if($tsCurrent == $ts) {
				$value = $in[$currentIndex][1];
			} else {
				$value = $in[$currentIndex - 1][1] +  ($ts - $tsPrevious)/($tsCurrent - $tsPrevious) * ($in[$currentIndex][1] - $in[$currentIndex-1][1]);
			}
				
			$out[] = array($ts, $value);
		}
			
		return $out;
	}

	/**
	 * 
	 * find index of first maximum (lower values before and after)
	 * @param numeric[] $array
	 */
	public static function realMaxIndex($array){
		// search index of maximum
		$maxvalue=max($array);
		while(list($key,$value)=each($array)){
			if($value==$maxvalue)$maxindex=$key;
		}

		if($maxindex == 0 && count($array) > 1) {
			array_splice($array, 1);
			$maxindex = self::realMaxIndex($array);
		}
			 
		return $maxindex;
	}

	/**
	 * 
	 * 3D-distance between 2 points
	 * @param Point / stdClass with x,y,z $p1
	 * @param Point / stdClass with x,y,z $p2
	 */
	public static function distance($p1, $p2) {
		$dx = $p1->x - $p2->x;
		$dy = $p1->y - $p2->y;
		$dz = $p1->z - $p2->z;
		return abs(sqrt(
				$dx*$dx + $dy*$dy + $dz*$dz
		));
	}
	
	/**
	 * 
	 * Length of a 3D-vector
	 * @param float[] $p1
	 */
	public static function length3D($p1) {
		$dx = $p1[0];
		$dy = $p1[1];
		$dz = $p1[2];
		return abs(sqrt(
				$dx*$dx + $dy*$dy + $dz*$dz
		));
	}
	
	/**
	 * 
	 * Projection of 3D-vector onto a given base/direction
	 * @param float[] $p1
	 * @param float[] $unitBase
	 */
	public static function project3D($p1, $unitBase) {
		return SPL::dot3D($p1, $unitBase);
	}
	
	/**
	 * 
	 * Addition of two 3D-vectors
	 * @param float[] $p1
	 * @param float[] $p2
	 */
	public static function add3D($p1, $p2) {
		return array($p1[0]+$p2[0], $p1[1]+$p2[1], $p1[2]+$p2[2]);
	}
	
	/**
	 * 
	 * Dot product between two 3D-vectors
	 * @param float[] $p1
	 * @param float[] $p2
	 */
	public static function dot3D($p1, $p2) {
		return $p1[0]*$p2[0] + $p1[1]*$p2[1] + $p1[2]*$p2[2];
	}
	
	/**
	 * 
	 * Scaling of a 3D-vector (multiplication with scaler)
	 * @param float $l
	 * @param float[] $p1
	 */
	public static function scale3D($l, $p1) {
		return array($l * $p1[0], $l * $p1[1], $l * $p1[2]);
	}
}

// make this class available as SignalProcessingLibrary, too
class_alias('SPL', 'SignalProcessingLibrary');
?>