<?php
if ( !defined('CP') ) { die("Invalid access.");}
$samplingTime = 5000;

if(isset($_POST['sample'])) {
	$sample = $_POST['sample'];
	var_dump($sample);

	$fp = fopen($dataFolder.date('Ymd_His').'.json', 'w');
	fwrite($fp, json_encode($sample));
	fclose($fp);
}

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" type="text/css" href="<?=FILE_PREFIX?>style.css">
<script type="text/javascript" src="<?=FILE_PREFIX?>jquery-1.9.1.min.js"></script>

<script type="text/javascript" src="<?=FILE_PREFIX?>cv.js"></script>
<script type="text/javascript" src="<?=FILE_PREFIX?>aruco.js"></script>
<script type="text/javascript">
	var video, canvas, context, imageData, detector;
	var canvasScreeny; var screenyLocked = false;

	// initiate frame clock
	(function() {
	  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
								  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	  window.requestAnimationFrame = requestAnimationFrame;
	})();

	// prepare canvas and get webcam video stream
	$(function() {
		video = document.getElementById("video");
	    canvas = document.getElementById("canvas");
	    canvasScreeny = document.getElementById("canvasScreeny");
	    context = canvas.getContext("2d");
	    contextScreeny = canvasScreeny.getContext("2d");

	    canvas.width = parseInt(canvas.style.width);
	    canvas.height = parseInt(canvas.style.height);

	    canvasScreeny.width = parseInt(canvasScreeny.style.width);
	    canvasScreeny.height = parseInt(canvasScreeny.style.height);
	      
	    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
	    if (navigator.getUserMedia){
			function successCallback(stream){
	      		if (window.webkitURL) {
	        		video.src = window.webkitURL.createObjectURL(stream);
	      		} if (navigator.mozGetUserMedia) {
					video.mozSrcObject = stream;
				} else {
	        		video.src = stream;
	      		}
				video.play();
				
	    	}
			
			function errorCallback(error){}
		
	    	navigator.getUserMedia({video: true}, successCallback, errorCallback);
	      
	    detector = new AR.Detector();
	    requestAnimationFrame(tick);
		}
	});

	// process frame
	function tick(){
		requestAnimationFrame(tick);

		if (video.readyState === video.HAVE_ENOUGH_DATA){
	    	snapshot();

	    	var markers = detector.detect(imageData);
	    	if(markers.length == 0 && !screenyLocked) {
				takeScreeny();
	    	}
	    	
	    	drawCorners(markers);
	    	drawId(markers);
	   	}
    }

	// take snapshot from canvas
	function snapshot(){
		context.drawImage(video, 0, 0, canvas.width, canvas.height);
		imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	}

	// take screenshot from canvas
	function takeScreeny() {
		//screenyLocked = true;
		contextScreeny.drawImage(video, 0, 0, canvasScreeny.width, canvasScreeny.height);
	}
	function clearScreeny() {
		screenyLocked = false;
	}

	// draw detected corners
    function drawCorners(markers){
      var corners, corner, i, j;
    
      context.lineWidth = 3;

      for (i = 0; i !== markers.length; ++ i){
        corners = markers[i].corners;
        
        context.strokeStyle = "red";
        context.beginPath();
        
        for (j = 0; j !== corners.length; ++ j){
          corner = corners[j];
          context.moveTo(corner.x, corner.y);
          corner = corners[(j + 1) % corners.length];
          context.lineTo(corner.x, corner.y);
        }

        context.stroke();
        context.closePath();
        
        context.strokeStyle = "green";
        context.strokeRect(corners[0].x - 2, corners[0].y - 2, 4, 4);
      }
    }

	// draw marker id
	function drawId(markers){
		var corners, corner, x, y, i, j;
		context.strokeStyle = "blue";
		context.lineWidth = 1;

		for (i = 0; i !== markers.length; ++ i){
            corners = markers[i].corners;

            x = Infinity;
            y = Infinity;

			for (j = 0; j !== corners.length; ++ j){
				corner = corners[j];

				x = Math.min(x, corner.x);
				y = Math.min(y, corner.y);
			}

			context.strokeText(markers[i].id, x, y);
		}
	}
	</script>

<script type="text/javascript">
	var console = window.console || { log: function() {} };

	/* estimate time offset between server and local device */
	<?php $serverTime = microtime(true)*1000;?>
	var localTimestamp = new Date().getTime();
	var serverOffset = <?=$serverTime?> - localTimestamp; 

	/* container for acceleration samples */
	var accData = new Array();
	var sampling = false;
	function addSample(sample) {
		sample.ts = new Date().getTime();
		accData.push(sample);
	}
	function clearSamples() {
		accData = new Array();
	}

	/* helper to update displayed message */
	function updateMessage(text) {
		$('#message').hide().html(text).fadeIn(200, function() {});
	}

	/* form submission */
	$(function() {
		$("#localTimestamp").html(localTimestamp);
		
		$("#auth_recording").click(function() {
			updateMessage('Sampling...');
			sampling = true;
			setTimeout(function() { sendData();}, <?=$samplingTime?>);

			return false;
		});

		$("#auth_stop_recording").click(function() {
			updateMessage('Sampling stopped.');
			sampling = false;
			return false;
		});
		
	});

	sendData = function() {
		//sampling = false;
		updateMessage("Sending...");
		var data = {
				sample: {
					name: 'webcam',
					comment: $('#auth_comment').val(),
					estimatedServerOffset: serverOffset,
					timestamp: (new Date().getTime()),
					acc: accData
				}
			};
		//alert (dataString);return false;
		
		$.ajax({
			type: "POST",
			url: "<?=FILE_PREFIX?>index.php?action=webcamrecording",
			data: data,
			success: function() {
				updateMessage("Sent");
			}
		});
		return false;
		clearSamples();
		setTimeout(function() { sendData();}, <?=$samplingTime?>);
	};
		
	function deviceMotionHandler(eventData) {
		var acceleration = eventData.acceleration;
		addSample(acceleration);
	  
	  // Display the raw acceleration data
	  var rawAcceleration = "[" +  Math.round(acceleration.x) + ", " + 
		Math.round(acceleration.y) + ", " + Math.round(acceleration.z) + "]";
	
	  // Display the acceleration and calculated values
	  document.getElementById("moAccel").innerHTML = rawAcceleration;
	  document.getElementById("moCalcInterval").innerHTML = eventData.interval;
	}
	</script>
</head>

<body style="width: 100%;">

<!-- rotating text and acc info -->
<div style="padding: 10px;">
<h2>Webcam Recording</h2>
Works at least in Chrome.
<table>
	<tr>
		<td>Server Time:</td>
		<td><?=$serverTime?></td>
	</tr>
	<tr>
		<td>Local Time:</td>
		<td id="localTimestamp"></td>
	</tr>
	<tr>
		<td>ACC:</td>
		<td id="moAccel"></td>
	</tr>
	<tr>
		<td>Interval:</td>
		<td id="moCalcInterval"></td>
	</tr>
</table>
</div>

<div style="padding: 10px;">
<div>Message: <span id="message"></span></div>
&nbsp;
<form method="post" action="">
<table>
	<tr>
		<td>Name:</td>
		<td><input name="auth_user" id="auth_user" type="text"
			value="<?php echo (isset($_POST['auth_user']) ? $_POST['auth_user'] : ''); ?>"></td>
	</tr>
	<tr>
		<td>Kommentar:</td>
		<td><input name="auth_comment" id="auth_comment" type="text"
			value="<?php echo (isset($_POST['auth_comment']) ? $_POST['auth_comment'] : ''); ?>"></td>
	</tr>
</table>
<input name="start_recording" id="auth_recording" type="submit"
	value="Start Recording"><input name="start_stop_recording"
	id="auth_stop_recording" type="submit" value="Stop Recording"></form>
</div>

<video id="video" autoplay="true" style="display:none;"></video>
<a href="#" onclick="clearScreeny(); return false;">clear screeny</a>
<canvas id="canvas" style="width:640px; height:480px;float: left;"></canvas>
<canvas id="canvasScreeny" style="width:640px; height:480px;"></canvas>


<!-- <div id="disp"><canvas id="comp"></canvas></div>
<video id="video" autoplay width="300"></video>
<canvas id="canvas" style="width:300px;"></canvas>
<script type="text/javascript" src="<?=FILE_PREFIX?>webcam_recorder.js"></script>-->
</body>
</html>
