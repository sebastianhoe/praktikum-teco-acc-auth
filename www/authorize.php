<?php
/**
 * Check if current user is authorized to access the internet based on the computed matching result.
 */
$data = new stdClass();
$data->accepted = false;
$data->message = 'Not authorized.';
$data->score = null;

$authConfig = isset($config['authentication']) ? $config['authentication'] : array();

// Threshold to accept the user
$threshold = isset($authConfig['threshold']) ? $authConfig['threshold'] : 0.5;

$sessionIdCheck = true;

if(DEBUG && isset($_GET['f']) && $_GET['f']) {
	$_SESSION['filename'] = $_GET['f'];
	$sessionIdCheck = false;
}

// check if filename for current login try has been set
if(isset($_SESSION['filename'])) {
	$filename = $_SESSION['filename'];
	
	// check if session_id matches (contained in filename)
	if(!$sessionIdCheck || strpos($filename, session_id()) !== false) {
		$matcher = new Matcher();
		if(isset($authConfig['patternStartTrim'])) $matcher->usedPatternOffset = $authConfig['patternStartTrim'];
		if(isset($authConfig['metric'])) $matcher->usedMetric = $authConfig['metric'];
		if(isset($authConfig['smoothingTau'])) $matcher->smoothingTau = $authConfig['smoothingTau'];
		if(isset($authConfig['clockOffsetLimit'])) $matcher->offsetLimit = $authConfig['clockOffsetLimit'] / 1000;
		if(isset($authConfig['referenceProperty'])) $matcher->referenceProperty = $authConfig['referenceProperty'];
		if(isset($authConfig['patternProperty'])) $matcher->patternProperty = $authConfig['patternProperty'];
		
		$pattern = new Dataset();
		$pattern->load($filename);
		
		$patternTimestamp = Dataset::timestampFromFilename($filename);
		
		$results = $matcher->searchMatch($pattern, 'visual');
		
		$bestMatch = 0;
		if(is_array($results)) {
			foreach($results as $result) {
				if($bestMatch < $result['bestMatch']) {
					$bestMatch = $result['bestMatch'];
				}
			}
		}
		if($bestMatch > 0) {
			$data->score = $bestMatch;
		}
		if($bestMatch > $threshold) {
			$data->accepted = true;
			$data->message = 'Authorized.';
			
		}
	}
	
}

echo json_encode($data);

if($data->accepted) {
	// tell pfsense to let user pass
	// Marcel Köpke figured out, which files to include and which function to call for pfsense during his soundlogin work.
	if(file_exists("auth.inc")) {
		require_once("auth.inc");
		require_once("functions.inc");
		require_once("captiveportal.inc");
	
		$clientip = $_SERVER['REMOTE_ADDR'];
		$clientmac = arp_get_mac_by_ip($clientip);
		global $redirurl;
		$redirurl = "http://www.kit.edu";
		portal_allow($clientip, $clientmac,"acc_auth");
	}
}
exit;
?>