<?php
/**
 * The analysis-service is used to load time series and determine correlation and lag between them
 * for the analysis-user-interface.
 */
if ( !defined('CP') ) { die("Invalid access.");}

$datasets = array();
$data = new stdClass();
$data->series = array();

// load desired signals from datasets to be displayed
// deprecated: the code within this foreach-bracketing is based on deprecated code, better use Dataset-class.
foreach($_POST as $key => $value) {
	if(false !== strpos($key, '_json')) {
		// json-file found, value should be array of requested parameters
		$filepath = str_replace(array('..', '_json'), array('', '.json'), $key);
		$datasets[$filepath] = json_decode(file_get_contents($dataDirectory.$filepath));
		$d = $datasets[$filepath];
		
		// compute acceleration if requested and position values available
		$tdataset = new Dataset();
		if(isset($d->pos) && 
			((isset($_POST['compute_acceleration']) && $_POST['compute_acceleration']) || !isset($d->acc))) {
			$tdataset->setPosition($d->pos);
			$tdataset->computeAcceleration();
			$d->acc = $tdataset->getAcceleration();
		}
		
		// filter artifacts (value overflows) from acceleration signal
 		$d->acc = $tdataset->filterAcceleration($d->acc);
 		if(isset($d->accG)) {
 			$d->accG = $tdataset->filterAcceleration($d->accG);
 		}
		
		if(!isset($d->estimatedServerClockLag)) {
			$d->estimatedServerClockLag = 0;
		}
		
		// move signal to t=0 or move to actual timestamp if time series timestamps are too small (less than a year after 1970)
		if(isset($_POST['move_to_zero']) && $_POST['move_to_zero']) {
			$d->estimatedServerClockLag = +$d->acc[0]->ts;
		} else if($d->acc[0]->ts < 1000*60*60*24*365) {
 			$d->estimatedServerClockLag = -(-$d->acc[0]->ts + $d->timestamp - ($d->acc[count($d->acc)-1]->ts - $d->acc[0]->ts));
 		}
 		
		foreach($value as $valueType => $valueTypeValue) {
			if(in_array($valueType, array('cema'))) {
				continue;
			}
			$series = new stdClass();
			$series->label = $d->name.'/'.$d->comment.' '.($d->timestamp - $d->estimatedServerClockLag).' '.$valueType;
			$series->type = $valueType;
			$values = array();
			switch ($valueType) {
				case 'x': 
					foreach($d->acc as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->x);
					}
					//$series->color = 'rgb(220, 50, 0)';
					break;
				case 'y':
					foreach($d->acc as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->y);
					}
					//$series->color = 'rgb(220, 160, 0)';
					break;
				case 'z':
					foreach($d->acc as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->z);
					}
					//$series->color = 'rgb(220, 50, 130)';
					break;
				case 'xpos': 
					foreach($d->pos as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->x);
					}
					//$series->color = 'rgb(220, 50, 0)';
					break;
				case 'ypos':
					foreach($d->pos as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->y);
					}
					//$series->color = 'rgb(220, 160, 0)';
					break;
				case 'zpos':
					foreach($d->pos as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->z);
					}
					//$series->color = 'rgb(220, 50, 130)';
					break;
					
				case 'x+G': 
					foreach($d->accG as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->x);
					}
					//$series->color = 'rgb(0, 255, 0)';
					break;
				case 'y+G':
					foreach($d->accG as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->y);
					}
					//$series->color = 'rgb(130, 255, 0)';
					break;
				case 'z+G':
					foreach($d->accG as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->z);
					}
					//$series->color = 'rgb(0, 255, 130)';
					break;
				case 'alpha': 
					foreach($d->ori as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->alpha);
					}
					//$series->color = 'rgb(0, 0, 255)';
					break;
				case 'beta':
					foreach($d->ori as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->beta);
					}
					//$series->color = 'rgb(130, 0, 255)';
					break;
				case 'gamma':
					foreach($d->ori as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, $acc->gamma);
					}
					//$series->color = 'rgb(0, 130, 255)';
					break;
				case 'acc_norm':
					foreach($d->acc as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, sqrt($acc->x*$acc->x + $acc->y*$acc->y + $acc->z*$acc->z));
					}
					//$series->color = 'rgb(255, 0, 0)';
					break;
				case 'pc_proj_norm':
					$d->accPcProj = $tdataset->principalComponentProjection($d->acc);
					foreach($d->accPcProj as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, sqrt($acc->x*$acc->x + $acc->y*$acc->y + $acc->z*$acc->z));
					}
					break;
				case 'pc_proj_value':
						$d->accPcProj = $tdataset->principalComponentProjection($d->acc);
						$propertyPC = Dataset::principalComponent($d->acc, true);
						foreach($d->accPcProj as $accKey => $acc) {
							$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, SPL::project3D(array($acc->x, $acc->y, $acc->z), $propertyPC));
						}
						break;
				case 'accG_norm':
					foreach($d->accG as $accKey => $acc) {
						$values[] = array(isset($acc->ts) ? $acc->ts - $d->estimatedServerClockLag : $accKey, sqrt($acc->x*$acc->x + $acc->y*$acc->y + $acc->z*$acc->z));
					}
					//$series->color = 'rgb(255, 100, 0)';
					break;
				default;
			}
			
			// smoth signal
			if(isset($_POST['smooth']) && $_POST['smooth'] && count($values) > 0) {
				Dataset::computeIEMA($values,50);
			}
			
			$series->data = $values;
			$data->series[] = $series;
			
			// additionally compute the cema-smoothed signal if desired
			if(array_key_exists('cema', $value)) {
				$series = new stdClass();
				$series->label = $d->name.' '.($d->timestamp + $d->estimatedServerClockLag).' '.$valueType.'_cema';
				$series->type = 'cema';
			
				$numValues = array();
				$i = 0;
				foreach($values as $v) {
        			$numValues[$i] = $v[1];$i++;
    			}
				$numValues = SPL::cema($numValues, 0.3);
				$cemaValues = array();
				$i = 0;
				foreach($values as &$v) {
        			$cemaValues[] = array($v[0] ,$numValues[$i]); $i++;
    			}
    			$series->data = $cemaValues;
				$data->series[] = $series;
			} else {
//				$series->data = $values;
			}
			
		}
	}
}

// correlate signals and determine lag
if((isset($_POST['compute_correlation']) && $_POST['compute_correlation']) && (count($data->series) == 2 || count($data->series) == 4)) {
	
	// parameters for the signal matcher
	$resolution = isset($_POST['resolution']) ? $_POST['resolution'] : 33; // milliseconds sampling interval
	$maxGap = isset($_POST['maximum_signal_gap']) ? $_POST['maximum_signal_gap'] : 150;
	$window = isset($_POST['search_window']) ? $_POST['search_window'] : 3000; // milliseconds
	$minOverlap = isset($_POST['minimum_overlap']) ? $_POST['minimum_overlap'] : 8000;
	$usedPatternLength = isset($_POST['pattern_length']) ? $_POST['pattern_length'] : 10000000;
	$usedPatternOffset = isset($_POST['pattern_offset']) ? $_POST['pattern_offset'] : 0;
	$usedReferenceOffset = isset($_POST['reference_offset']) ? $_POST['reference_offset'] : 0;
	$usedMetric = isset($_POST['used_metric']) ? $_POST['used_metric'] : 'imseMirror';
	$smoothingTau = isset($_POST['smoothing_tau']) ? $_POST['smoothing_tau'] : 0;
	
	// apply parameters
	$matcher = new Matcher();
	$matcher->resolution = $resolution;
	$matcher->maximumGapLength = $maxGap;
	$matcher->searchWindow = $window;
	$matcher->minimumOverlap = $minOverlap;
	$matcher->usedPatternLength = $usedPatternLength;
	$matcher->usedPatternOffset = $usedPatternOffset;
	$matcher->usedReferenceOffset = $usedReferenceOffset;
	$matcher->usedMetric = $usedMetric;
	$matcher->smoothingTau = $smoothingTau;
	$matcher->searchOnlyInWindow = false;
	
	$crosscorrelation = null;
	if(count($datasets) == 2) {
		$indexZeroIsReference = false;
		
		$filenames = array_keys($datasets);
		if(substr($filenames[0],0,6) == 'visual') {
			$indexZeroIsReference = true;
		}
		
		// load pattern dataset
		$patternDataset = new Dataset();
		$patternDataset->load($filenames[$indexZeroIsReference ? 1 : 0]);
		$data->patternFile = $patternDataset->getFilename();
		$data->patternInfo = new stdClass();
		$data->patternInfo->name = $patternDataset->getName();
		$data->patternInfo->comment = $patternDataset->getComment();
		$data->patternInfo->estimatedServerClockLag = $patternDataset->getEstimatedServerClockLag();
		$data->patternInfo->timestamp = $patternDataset->getTimestamp();
		
		// load reference dataset
		$referenceDataset = new Dataset();
		$referenceDataset->load($filenames[$indexZeroIsReference ? 0 : 1]);
		$data->referenceFile = $referenceDataset->getFilename();
		$data->referenceInfo = new stdClass();
		$data->referenceInfo->name = $referenceDataset->getName();
		$data->referenceInfo->comment = $referenceDataset->getComment();
		$data->referenceInfo->estimatedServerClockLag = $referenceDataset->getEstimatedServerClockLag();
		$data->referenceInfo->timestamp = $referenceDataset->getTimestamp();
		
		// compute correlation and determine lag
		$referenceKey = str_replace('.json', '_json', $filenames[$indexZeroIsReference ? 0 : 1]);
		$patternKey = str_replace('.json', '_json', $filenames[$indexZeroIsReference ? 1 : 0]);
		$referenceKeys = array_keys($_POST[$referenceKey]);
		$patternKeys = array_keys($_POST[$patternKey]);
		$options = array(
				'referenceProperty' => $referenceKeys[0],
				'patternProperty' => $patternKeys[0],
				);
		
		$result = $matcher->match($patternDataset, $referenceDataset, $options);
		
		$crosscorrelation = $result['matchingScores'];
		$patternTimeSeries = $result['pattern'];
		$referenceTimeSeries = $result['reference'];
		$startingOffset = $result['startingOffset'];
		
		// make time series show in graph
		if(isset($_POST['show_resampled_data'])) {
			if(count($data->series) == 2) {
				$data->series[$indexZeroIsReference ? 1 : 0]->data = $patternTimeSeries;
				$data->series[$indexZeroIsReference ? 0 : 1]->data = $referenceTimeSeries;
			} else {
				$data->series[$indexZeroIsReference ? 2 : 0]->data = $patternTimeSeries;
				$data->series[$indexZeroIsReference ? 0 : 2]->data = $referenceTimeSeries;
			}	
		}
		
		// gather results
		$data->isPair = ($patternDataset->getPairId() != null && $patternDataset->getPairId() == $referenceDataset->getPairId());
		$data->matching = $crosscorrelation;
		$data->matchIndex = $result['matchingIndex'];
		$data->bestMatch = $result['bestMatch'];
		$data->startOffset = $startingOffset;
		$data->matcher = $matcher;
	}
	
	// fix offset
	if(isset($_POST['show_resampled_data'])) {
		//$startingOffset = 0;
	} else {
		$startingOffset = 0;
	}
	
	// move pattern if crosscorrelation found)
	if(count($crosscorrelation) > 0) {
		$lag = -SPL::realMaxIndex($crosscorrelation);
		$data->lag = $lag;
		$data->movedBy = -SPL::realMaxIndex($crosscorrelation);
		
		// overwrite raw data with resampled data if requested
		if(isset($_POST['show_resampled_data'])) {
			$referenceStartIndex = 0;
			for($referenceStartIndex = 0; $referenceStartIndex  < count($referenceTimeSeries); $referenceStartIndex++) {
				if($referenceTimeSeries[$referenceStartIndex][0] + $lag > $patternTimeSeries[0][0]) {
					break;
				}
			}
			
			//$referenceTimeSeries = array_values(SPL::normalize($referenceTimeSeries, $referenceStartIndex, $referenceStartIndex + count($patternTimeSeries)));
			$referenceTimeSeries = array_values($referenceTimeSeries);
			
			if(count($data->series) == 2) {
				$data->series[$indexZeroIsReference ? 0 : 1]->data = $referenceTimeSeries;
			} else {
				$data->series[$indexZeroIsReference ? 0 : 2]->data = $referenceTimeSeries;
			}	
		}
		
		// move pattern
		for($i = 0, $ic = count($data->series[$indexZeroIsReference ? 1 : 0]->data); $i < $ic; $i++) {
			$data->series[$indexZeroIsReference ? 1 : 0]->data[$i][0] -= $lag;
		}
		
		// add crosscorrelation result to graph
		$series = new stdClass();
		$series->label = 'matching';
		$series->type = 'matching';
		$values = array();
		reset($referenceTimeSeries);
		
		foreach($crosscorrelation as $key => $v) {
			$values[] = array($referenceTimeSeries[key($referenceTimeSeries)][0]+$key, $v);
		}
		$series->data = $values;
		$data->series[] = $series;
	}
}

// send results
echo json_encode($data);
?>