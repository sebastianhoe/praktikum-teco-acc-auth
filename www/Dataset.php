<?php
class Dataset {
	protected $filename;
	protected $timestampFromFilename;
	
	protected $data;
	
	protected static $dataDirectory = 'data/';
	
	public function __construct() {
		$this->data = new stdClass();
	}
	
	public static function setDataDirectory($directory) {
		self::$dataDirectory = $directory;
	}
	
	public static function listDatasets($directory = '', $recursive = false) {
		$files = array();
		$fileType = array();
		if(strlen($directory) > 0 && substr($directory, -1, 1) != '/') {
			$directory .= '/';
		}
		$dir = self::$dataDirectory.$directory;
		$type = strlen($directory) > 0 ? substr($directory,0,-1) : 'default';
		if(!file_exists($dir)) { return array(); }
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			if($filename != '.' && $filename != '..') {
				//echo $dir.$filename.'<br/>';
				if(is_dir($dir.$filename)) {
					if($recursive) {
						$files = array_merge($files, self::listDatasets($filename, $recursive));
					}
				} else {
					$timestamp = self::timestampFromFilename($filename);
					$files[$filename] = array('filename' => $filename, 'timestamp' => $timestamp, 'type' => $type, 'path' => $directory.$filename);
				}
			}
		}
		ksort($files);
		return $files;
	}
	
	public static function computeIEMA(&$series, $tau = 300) {
		$timestamps = array();
		$valuesX = array();
		$valuesY = array();
		$valuesZ = array();
		if(is_array($series[0])) {
			foreach ($series as $a) {
				$timestamps[] = $a[0];
				$valuesX[] = $a[1];
			}
			$valuesX = SPL::iema($timestamps, $valuesX, $tau);
	
			for($i=0, $ic = count($series); $i < $ic; $i++) {
				$series[$i][1] = $valuesX[$i];
			}
	
		} else {
			foreach ($series as $a) {
				$timestamps[] = $a->ts;
				$valuesX[] = $a->x;
				$valuesY[] = $a->y;
				$valuesZ[] = $a->z;
			}
			$valuesX = SPL::iema($timestamps, $valuesX, $tau);
			$valuesY = SPL::iema($timestamps, $valuesY, $tau);
			$valuesZ = SPL::iema($timestamps, $valuesZ, $tau);
			$i = 0;
			foreach ($series as $a) {
				$a->x = $valuesX[$i];
				$a->y = $valuesY[$i];
				$a->z = $valuesZ[$i];
				$i++;
			}
		}
	}
	
	public function computeAcceleration() {
		$pos = $this->getPosition();
		if(is_null($pos)) return false;
		$acc = array();
		$vel = array();
		
		$scaling = 1.4; // depending on smoothing, 1.3-1.5 work good if smoothing is enabled
	
		Dataset::computeIEMA($pos, 45.0);
		
		for($i = 1, $steps = count($pos)-1; $i < $steps; $i++) {
			$a = new stdClass();
	
			$pprev = $pos[$i-1];
			$p = $pos[$i];
			$ppost = $pos[$i+1];
			$dtprev = abs($pprev->ts - $p->ts) / 1000.0;
			$dtpost = abs($p->ts - $ppost->ts) / 1000.0;
	
			$vprev_x = ($p->x - $pprev->x) / $dtprev;
			$vprev_y = ($p->y - $pprev->y) / $dtprev;
			$vprev_z = ($p->z - $pprev->z) / $dtprev;
	
			$vpost_x = ($ppost->x - $p->x) / $dtpost;
			$vpost_y = ($ppost->y - $p->y) / $dtpost;
			$vpost_z = ($ppost->z - $p->z) / $dtpost;
	
			$dtv = ($dtprev + $dtpost) / 2.0;
	
			$a->ts = $p->ts;
			$a->x = $scaling * ($vpost_x - $vprev_x) / $dtv;
			$a->y = $scaling * ($vpost_y - $vprev_y) / $dtv;
			$a->z = $scaling * ($vpost_z - $vprev_z) / $dtv;
// 					$a->x = $vprev_x;
// 					$a->y = $vprev_y;
// 					$a->z = $vprev_z;
	
			$acc[] = $a;
			//var_dump($a);
		}
		//Dataset::computeIEMA($acc, 1.0);
		$this->setAcceleration($acc);
	}
	
	public function load($file) {
		$file = self::sanitizeDirectory($file);
		$file = substr($file, 0, -1); // remove added "/" from directory sanitization
		
		if(!file_exists(self::$dataDirectory.$file)) {
			return false;
		}
		
		$this->filename = $file;
		$this->data = json_decode(file_get_contents(self::$dataDirectory.$file));
		
		// fix old kinect timestamps starting at kinect initialization
		if(isset($this->data->pos) && $this->data->pos[0]->ts < 1000*60*60*24*365) {
			$this->data->estimatedServerClockLag = -(-$this->data->pos[0]->ts + $this->data->timestamp - ($this->data->pos[count($this->data->pos)-1]->ts - $this->data->pos[0]->ts));
		}
		
		$this->data->acc = $this->filterAcceleration($this->getAcceleration());
		$this->data->accG = $this->filterAcceleration($this->getAccelerationWithG());
		$this->timestampFromFilename = self::timestampFromFilename($file);
		
		return true;
	}
	
	public static function timestampFromFilename($filename) {
		$timestamp = null;
		if(preg_match('/(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2})/', $filename, $matches)) {
			$year = $matches[1]; $day = $matches[3]; $month = $matches[2]; $hours = $matches[4]; $minutes = $matches[5]; $seconds = $matches[6];
			$timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
		}
		return $timestamp;
	}
	
	protected function sanitizeDirectory($directory) {
		if(strlen($directory) > 0 && substr($directory, -1, 1) != '/') {
			$directory .= '/';
		}
		// TODO add path sanitization code (realpath, inside datadirectory,...
		return $directory;
	}
	
	public function store($directory, $filename = '') {
		$directory = self::sanitizeDirectory($directory);
		$timestamp = time();
		if(strlen($filename) == 0) {
			$this->filename = date('Ymd_His', $timestamp).'_'.substr(microtime(),0,6).'.json';
		} else {
			$this->filename = $filename;
		}
		$this->timestampFromFilename = $timestamp;
		$fp = fopen(self::$dataDirectory.$directory.$this->filename, 'w');
		fwrite($fp, json_encode($this->data));
		fclose($fp);
	}

	public function getFilename() {
		return $this->filename;
	}

	public function getName() {
		if(isset($this->data->name)) {
			return $this->data->name;
		} else {
			return null;
		}
	}
	
	public function setName($value) {
		$this->data->name = $value;
	}
	
	public function getPairId() {
		if(isset($this->data->pairId)) {
			return $this->data->pairId;
		} else {
			return null;
		}
	}
	
	public function setPairId($value) {
		$this->data->pairId = $value;
	}
	
	public function getComment() {
		if(isset($this->data->comment)) {
			return $this->data->comment;
		} else {
			return null;
		}
	}
	
	public function setComment($value) {
		$this->data->comment = $value;
	}
	
	public function getEstimatedServerClockLag() {
		if(isset($this->data->estimatedServerClockLag)) {
			return $this->data->estimatedServerClockLag;
		} else {
			return null;
		}
	}
	
	public function setEstimatedServerClockLag($value) {
		$this->data->estimatedServerClockLag = $value;
	}
	
	public function getTimestamp() {
		if(isset($this->data->timestamp)) {
			return $this->data->timestamp;
		} else {
			return null;
		}
	}
	
	public function setTimestamp($value) {
		$this->data->timestamp = $value;
	}
	
	public function getAcceleration($index = null) {
		if(isset($this->data->acc)) {
			if(!is_null($index)) {
				if(is_array($this->data->acc) && isset($this->data->acc[$index])) {
					return $this->data->acc[$index];
				} else {
					return null;
				}
			}
			return $this->data->acc;
		} else {
			return null;
		}
	}
	
	public function filterAcceleration($acc) {
		if($acc != null) {
			// filtered artifacts on positive peaks	
			$max = array('x' => 0, 'y' => 0, 'z' => 0);
			$maxArtifactLength = 200;
			$minArtifactLength = 30;
			$maxSearchLength = 500;
			for($i = 2, $accCount = count($acc); $i < $accCount; $i++) {
				$a =& $acc[$i];
				$dims = array('x', 'y', 'z');
				
				// filter overflows (jump > 2g and inverted sign)
				for($dimc = 0; $dimc < 3; $dimc++) {
					$dim = $dims[$dimc];
					
					if($a->{$dim} < 0 && $acc[$i-1]->{$dim} > 0 && $acc[$i-1]->{$dim} - $a->{$dim} > 20) {
						$holdValue = $acc[$i-1]->{$dim};
						$j = $i;
						while($j < $accCount && $acc[$j]->{$dim} < 0) {
							$acc[$j]->{$dim} = $holdValue;
						}
						//$i = $j + 1;
					} else if($a->{$dim} > 0 && $acc[$i-1]->{$dim} < 0 && $acc[$i-1]->{$dim} - $a->{$dim} < -20) {
						$holdValue = $acc[$i-1]->{$dim};
						$j = $i;
						while($j < $accCount && $acc[$j]->{$dim} > 0) {
							$acc[$j]->{$dim} = $holdValue;
						}
						//$i = $j + 1;
					}
				}
			}
			return $acc;
		} else {
			return null;
		}
	}
	
	public static function principalComponentProjection($acc) {
		$r = self::principalComponent($acc);
		
		$rLength = SPL::length3D($r);
		$rUnit = SPL::scale3D(1.0/$rLength, $r);
		
		foreach($acc as $key => $a) {
			$av = array($a->x, $a->y, $a->z);
			$res = SPL::scale3D(SPL::dot3D($av, $r)/$rLength, $rUnit); // res = (|a|cos(phi))* b/|b| = (a.b)/|b| * b/|b|
			
			$acc[$key]->x = $res[0];
			$acc[$key]->y = $res[1];
			$acc[$key]->z = $res[2];
		}
		
		return $acc;
	}
	
	public static function principalComponent($acc, $unitLength = false) {
		// compute first eigenvalue using the power iteration algorithm (also known as Von Mises iteration)
		$r = array(1,1,1); // random vector of length p, will hold the final result
		for($c = 0; $c < 5; $c++) {
			$s = array(0,0,0);
			foreach ($acc as $a) {
				$av = array($a->x, $a->y, $a->z);
				$s = SPL::add3D($s, SPL::scale3D(SPL::dot3D($av, $r), $av));
				$r = SPL::scale3D(1.0/SPL::length3D($s), $s);
			}
		}
		
		if($unitLength) {
			$r = SPL::scale3D(1.0/SPL::length3D($r), $r);
		}
	
		return $r;
	}
	
	public function setAcceleration($value) {
		$this->data->acc = $this->filterAcceleration($value);
	}
	
	public function getAccelerationWithG($index = null) {
		if(isset($this->data->accG)) {
			if(!is_null($index)) {
				if(is_array($this->data->accG) && isset($this->data->accG[$index])) {
					return $this->data->accG[$index];
				} else {
					return null;
				}
			}
			return $this->data->accG;
		} else {
			return null;
		}
	}
	
	public function setAccelerationWithG($value) {
		$this->data->accG = $this->filterAcceleration($value);
	}
	
	public function getOrientation($index = null) {
		if(isset($this->data->ori)) {
			if(!is_null($index)) {
				if(is_array($this->data->ori) && isset($this->data->ori[$index])) {
					return $this->data->ori[$index];
				} else {
					return null;
				}
			}
			return $this->data->ori;
		} else {
			return null;
		}
	}
	
	public function setOrientation($value) {
		$this->data->ori = $value;
	}
	
	public function getPosition($index = null) {
		if(isset($this->data->pos)) {
			if(!is_null($index)) {
				if(is_array($this->data->pos) && isset($this->data->pos[$index])) {
					return $this->data->pos[$index];
				} else {
					return null;
				}
			}
			return $this->data->pos;
		} else {
			return null;
		}
	}
	
	public function setPosition($value) {
		$this->data->pos = $value;
	}
}